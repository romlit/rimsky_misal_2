VEĽKONOČNý ŠTVRTOK

 

ÚVODNY SPEV (Múd IO, 20-21)

Pane, všetci si pochvaľujú tvoju mocnú ruku,
lebo múdrosť otvorila ústa nemým
a rozviazala jazyk maličkých. Aleluja.

Oslavná pieseň.

MODLITBA DNA

Bože a Otče náš,

ty si zjednotil rozmanité národy

vo vyznávaní tvojho mena; *

prosíme t'a, —

daj, aby všetkých,

čo sa znovuzrodili v krstnom prameni,

spájala živá viera a činorodá láska.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery sa vynechá.

NAD OBETNYMI DARMI

Dobrotivý Bože, *

prijmi naše obetné dary,

ktoré ti prinášame na znak vďaky
za novopokrstených, —

a sprevádzaj nás všetkých otcovskou priazňou.
Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočné I. (ale slávnostnejšie v tento deň) (str. 367).

Ak sa recituje Rímsky kánon, použije sa vlastná vložka V sýoločenstve a
Prosíme ťa Bože.

VEĽKONOČNY ŠTVRTOK 211

