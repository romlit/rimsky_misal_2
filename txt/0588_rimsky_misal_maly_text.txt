23 . januára

SV. ]ÁNA ALMUŽNÍKA

Spomienka (len v kostole sv. Martina v Bratislave)

Spoločná omša duchovných pastierov (str. 731 11.)
alebo spoločná omša svätých, čo konali skutky milosrdenstva (str. 762).

MODLITBA DNA

Všemohúci Bože, ty si svätého biskupa

]ána Almužníka obdaril nevšednou láskou

voči biednym a núdznym; *

na jeho príhovor nám pomôž, —

aby sme sa vymanili z otroctva lakomstva

a ochotne konali skutky milosrdenstva.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Dobrotivý Bože, nech sú ti milé obetné dary,
ktoré prinášame na oltár; *

prosíme t'a, —

nauč nás správne hodnotiť pozemské veci,
aby nám neprekážali v úsilí

o duchovné dobrá.

Skrze Krista, nášho Pána.

PO PRIJÍMANÍ

Pane a Bože náš,
nech toto prijímanie roznieti v nás takú lásku, *

488 VLASTNÉ OMŠE svÁTýCH

