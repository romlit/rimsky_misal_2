ako tí možno oddane slúžiť

v každom povolaní; *

láskavo nám pomáhaj, —

aby sme Kristovou náukou zveľaďovali svornost'
a upevňovali lásku medzi ľuďmi.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

SV. CYRILA ALEXANDRIJSKÉHO,
BISKUPA A UČITEĽA CIRKVI

Spoločná omša duchovných pastierov-biskupov (str. 728 n.)
alebo učiteľov Cirkvi (str. 743 n.).

MODLITBA DNA

Večný Bože,

vo svätom Cyrilovi si dal Cirkvi

neohrozeného obrancu pravdy,

že preblahoslavená Panna Mária je Bohorodičkou; *
aj my veríme a vyznávame,

že je naozaj Matkou Božou, _

a prosíme, aby sme dosiahli spásu

v tvojom Synovi Ježišovi Kristovi,

ktorý sa pre nás stal človekom

a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

28. júna
SV. IRENEJA, BISKUPA A MUČENÍKA

Spomienka

Spoločná omša mučeníkov (str. 713 n.)
alebo duchovných pastierov-biskupov (str. 728 n.).

554 VLASTNÉ OMŠE SVÁTYCH

