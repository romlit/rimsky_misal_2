MODLITBA DNA

Láskavý Bože,

ty osvecuješ svoju Cirkev náukou

svätého kňaza Bédu; *

prosíme t'a, dopraj, -—

aby nám jeho múdrosť svietila na ceste života

a jeho zásluhy pomáhali vytrvat' v dobrom.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

SV. GREGORA VII., PÁPEŽA

Spoločná omša duchovných pastierov-pápežov (str. 725 n.).

MODLITBA DNA

Všemohúci Bože,

svätého pápeža Gregora si vyznačil

velkou duchovnou silou

a horlivosťou za spravodlivosť ; *

udel' aj dnes svojej Cirkvi odvahu

vystupovať proti bezpráviu —

a s úprimnou láskou slobodne slúžiť pravde a dobru.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

SV. MÁRIE MAGDALÉNY DE“ PAZZI, PANNY

Spoločná omša panien (str. 746 n.)
alebo svätých rehoľníkov (str. 759 n.).

534 VLASTNÉ OMŠE SVÁTYCH

