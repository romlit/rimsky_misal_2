27. septembra
SV. VINCENTA DE PAUL, KNAZA

Spomienka

ÚVODNY SPEV (Lk 4, 18)

Duch Pánov je nado mnou, pretože ma pomazal,
aby som hlásal blahozvesť chudobným
a uzdravoval skrúšených srdcom.

MODLITBA DNA

Milosrdný Bože,

svätého Vincenta si obdaril apoštolskými čnost'ami,
aby sa staral o chudobných

a o výchovu dobrých kňazov; *

prosíme ťa, naplň aj nás apoštolskou horlivosťou, —
aby sme sa riadili jeho naučeniami

a nasledovali ho v láske k tebe a k blížnym.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom svätým po všetky veky vekov.

NAD OBETNýMI DARMI

Dobrotivý Bože,

svätému Vincentovi si dal milosť
životom uskutočňovať,

čo slávil v týchto svätých tajomstvách; *
vrúcne ťa prosíme, —

mocou tejto obety premeň aj nás

na príjemný dar pre teba.

Sere Krista, nášho Pána.

27. SEPTEMBRA 627

