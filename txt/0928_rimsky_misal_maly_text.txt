PO PRIJÍMANÍ

Láskavý Otče,

prijali sme telo a krv tvojho Syna,

ktoré si nám poskytol

na milej oslave tohto Výročia; *

daj, prosíme, —

aby náš brat M. (naša sestra M.),
posilnený(á) nebeským pokrmom a nápojom,
šťastlivo pokračoval(a) na ceste,

ktorá vedie k tebe.

Skrze Krista, nášho Pána.

828 OMŠE PRI VYSLUHOVANÍ SVÁTENÍN

