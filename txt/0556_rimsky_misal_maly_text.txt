ZÁVEREČNÉ OBRADY

Ak treba veriacim niečo oznámiť, možno to urobiť teraz.

Nasleduje rozpustenie zhromaždenia. Kňaz sa obráti k ľudu a s rozo-
pätými rukami povie:

Pán s vami.

Ľud odpovie:
I s duchom tvojím.

Kňaz požehná l'ud slovami:

Nech vás žehná všemohúci Boh,
Otec i Syn &! i Duch Svätý.

Ľud odpovie :
Amen.

%

Kňaz: Nech vás žehná vše-mo- hú— ci Boh,

ŠtB—_—

Otec i Syn i Duch Svä- tý.

%

Ľud; A- men.

 

V niektoré dni a pri niektorých príležitostiach možno miesto tohto iedno-
duche'ho požehnania použíť slávnostnú formulu požehnania (pozri str.
458-470), alebo modlitbu nad ľudom so záverečným požehnaním (pozri

str. 47 I -480).

Ak svätú omšu celebroval biskup, požehnanie udelí takto:
Biskup: Nech je zvelebené meno Pánovo.
Ľud: Od tohto času až naveky.

456 OMŠOVY PORIADOK

