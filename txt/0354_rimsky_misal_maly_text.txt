Pieseň vďaky veľkonočné (str. 367 n.).

SPEV NA PRIJÍMANIE (Jn zo, 19)

Ježiš si stal do stredu a povedal učeníkom:
Pokoj vám. AJeluja.

PO PRIJÍMANÍ

Prosíme ťa, večný Bože, *

láskavo zhliadni na svoj ľud,

ktorý si duchovne obnovil
veľkonočnými sviatosťami, —

a priveď nás k slávnemu vzkrieseniu.
Skrze Krista, nášho Pána.

UTOROK
po Šiestej veľkonočnej nedeli

 

ÚVODNY SPEV (Ziv 19, 7- 6)

Radujme sa, veseľme sa a oslavujme Boha,
lebo náš Pán, Boh všemohúci,
sa ujal kráľovstva. Aleluja.

MODLITBA DNA

Všemohúci a večný Bože,

tvoj ľud sa teší z prijatia do Božej rodiny; *

daj, nech sa, duchovne obnovený, stále raduje —
a s pevnou nádejou očakáva

deň svojho vzkriesenia.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

254 VEĽKONOČNÉ OBDOBIE

