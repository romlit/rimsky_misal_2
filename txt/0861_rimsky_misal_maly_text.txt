SPEV NA PRIJÍMANIE (Porov. Mt 19, 27-29)

Veru, hovorím vám: Vy, čo ste opustili všetko
a nasledovali ste ma,

stonásobne viac dostanete

a dosiahnete večný život.

PO PRIJÍMANÍ

Všemohúci Bože,

touto sviatosťou nám poskytuješ pomoc a silu; *
prosíme t'a, nauč nás, —

aby sme podľa príkladu svätého M. (svätej M.)
hľadali nadovšetko teba

a v tomto svete žili ako noví ľudia.

Skrze Krista, nášho Pána.

8. Omša o rehoľníkoch

ÚVODNY SPEV (Porov. Ž 23, 5—6)

Oslavujeme svätých, ktorí dostali požehnanie od Pána
a odmenu od Boha, svojho Spasiteľa,
lebo sú z pokolenia, čo hľadá Boha.

MODLITBA DNA

Večný Bože,

ty si svätého M. (svätú M.) povolal,

aby úsilím 0 dokonalú lásku

už na tomto svete hľadal(a) tvoje kráľovstvo; *

na jeho (jej) príhovor nás posilňuj, —

aby sme s duchovnou radosťou

napredovali na ceste lásky.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

SVÁTYCH MUŽOV A ŽIEN 761

