SLÁVNOST PANNY
MARIE, BOHORODIČKY

Drahí bratia a sestry, dnes na za-
čiatku nového občianskeho roku
modlime sa všetci spoločne k ne-
beskému Otcovi, aby na orodova-
nie Matky Božej udelil svetu pokoj.

* Za celú rodinu ľudstva — aby
sa zveľaďovala v láske, pokoji,
slobode a spravodlivosti, prosme
Pána.

* Za Svätého Otca — aby jeho
výzvy k pokoju a bratstvu našli
ozvenu vo všetkých srdciach, pros-
me Pána.

* Za všetkých kresťanov — aby
ich úcta k Matke Božej utvrdzovala
v jednote a vzájomnom porozu-
mení, prosme Pána.

* Za nás všetkých — aby sme v
začínajúcom roku mohli pokorne
slúžiť Bohu a pokojne nažívať s
každým blížnym, prosme Pána.

Bože, ktorý si nám dal za Spasiteľa
Knieža pokoja, prosíme ťa, daj
nám ducha kresťanskej lásky,
naplň nás myšlienkami pokoja a
posilňuj nás svojou milosťou, aby
sme ti mohli slúžiť bez strachu
a neistoty po všetky dni svojho
života. Skrze Krista, nášho Pána.

ZJAVENIE PÁNA

Bratia a sestry, prosme nebeského
Otca za všetkých, ktorí hľadajú
Boha a jeho Syna Ježiša Krista.

* Za tých, čo ešte nepoznajú Kris-
ta — aby k nim prišli horliví
hlásatelia Božieho slova, prosme
Pána.

* Za správcov verejných vecí 'v—

1020 DODATOK

aby účinne viedli ľud k spoločné-
mu blahu a neodvádzali ho od
večnej vlasti, prosme Pána.

* Za trpiacich na tele i na duši
—-aby ich posilňovala viera v Kris-
ta a dary Ducha Svätého, prosme
Pána.

* Za nás všetkých — aby sme
sa usilovali uskutočňovať náuku
Druhého vatikánskeho cirkevného
snemu, a tak sa stali svedkami
pravej viery v modernom svete,
prosme Pána.

Všemohúci Bože, vyslyš naše mod-
litby, ktoré ti prednášame za všet—
kých l'udí, aby sme ako synovia
svetla kráčali vo svetle tvojej prav-
dy do nebeskej vlasti. Skrze Krista,

nášho Pána.

KRST KRISTA PÁNA

Bratia a sestry, prosme nášho Pána
ježiša Krista, ktorého meno prevy-
šuje všetky iné mená, za seba,
čo už veríme v neho, i za ostatných,
aby v neho uverili.

* Za svätú Cirkev — aby s veľkou
láskou a ochotou poslúchala milo-
vaného Božieho Syna, prosme Pána.
* Za všetky národy — aby vo
svetle viery poznali, že len v Kris-
tovi obsiahnu plný život, pros-
me Pána.

* Za všetkých kresťanov — aby si
uvedomovali, že krstom sa stali
Božími deťmi, a teda majú žiť
v synovskej oddanosti voči Bohu,
prosme Pána.

* Za nás všetkých — aby sme sa
z vďačnosti za krst, ktorý sme
prijali, živo zaujímali o misijné
dielo Cirkvi, prosme Pana.

