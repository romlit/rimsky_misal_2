23. októbra
sv. JÁNA KAPISTRÁNSKEHO, KNAZA

Svätá omša duchovných pastierov-misionárov (str. 738 n.).

MODLITBA DNA

Milosrdný Bože,

ty si povolal svätého Jána Kapistránskeho,

aby v krušných časoch posmeľoval kresťanský ľud; *
prosíme t'a, aj nám poskytuj bezpečnú ochranu —
a svoju Cirkev zachovaj v trvalom pokoji.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

24. októbra
SV. ANTONA MÁRIE CLARETA, BISKUPA

Spoločná omša duchovných pastierov-misionárov (str. 738 n.)
alebo biskupov (str. 728 n.).

MODLITBA DNA

Všemohúci Bože, *

ty si obdaril svätého biskupa Antona
veľkou láskou a trpezlivosťou

pri hlásaní evanjelia národom; -—
daj, aby sme aj my hľadali

najprv Božie kráľovstvo

a horlivo privádzali svojich blížnych
k tvojmu Synovi Ježišovi Kristovi,
ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

646 VLASTNÉ OMŠE SVÁTYCH

