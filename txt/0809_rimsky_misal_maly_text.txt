MODLITBA DNA

Všemohúci Bože,

dávaš nám svätú radosť

z víťazstva tvojich mučeníkov M. a M.; *

prosíme ťa, - »

nech nás ich príklad posilňuje vo viere a v čnostiach
a ich mocné orodovanie nech nám dodáva odvahy.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Alebo:

Všemohúci Bože, vrúcne t'a prosíme, *

nech nám orodovanie svätých mučeníkov M. a M.
získa tvoju priazeň _

a vyprosí odvahu vyznávať vieru v teba.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Dobrotivý Bože,

prijmi dary, ktoré ti tvoj ľud prináša

na pamiatku svätých mučeníkov M. a M., *

a daj, aby Eucharistia,

ktorá bola pre nich zdrojom odvahy

v prenasledovaní, —

aj nám dodávala sily v protivenstvách.

Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Mk 8, 35)

Pán Ježiš hovorí:
Kto stratí svoj život pre mňa a pre evanjelium, zachráni si ho.

SVÁTYCH MUČENÍKOV 709

