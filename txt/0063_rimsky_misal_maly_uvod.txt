sluhujúcich nielen vidieť, ale pri
použití dnešných technických pri-
strojov aj dobre počuť.

IX. 0 mieste pre zbor a organ
a pre iné hudobné nástroje

274. Zbor spevákov (schola canto-
rum) má sa umiestniť podľa dispo-
zície každého kostola tak, aby sa
jasne ukázalo jeho začlenenie, že
je totiž časťou zhromaždeného spo-
ločenstva a že má osobitnú úlohu,
aby mohol ľahko vykonávať svoju
liturgickú službu a aby sa jeho
jednotlivým členom lepšie umož-
nila plná účasť na omši, t. j. svia-
tostná účasť.86

275. Organ a iné dovolené hudobné
nástroje treba umiestniť na vhod-
nom mieste, aby mohli sprevádzať
zbor spevákov i spievajúci ľud a
aby ich mohli všetci dobre počuť
aj vtedy, keď nesprevádzajú spev.

X. Úschova Najsvätejšej sviatosti

276. Osobitne sa odporúča, aby
miesto úschovy Najsvätejšej svia-
tosti bolo v kaplnke vhodnej na
súkromnú poklonu a modlitbu ve-
riacich.87 Ak to tak- nemôže byť,
podľa štruktúry jednotlivých kos-
tolov a podľa zákonitých miest—
nych zvykov treba Sviatosť uložiť
v kostole na niektorom oltári alebo
aj mimo oltára, ale na čestnom a
náležite vyzdobenom mieste.88

277. Najsvätejšiu sviatosť treba u-
schovávat' v jedinom svätostánku,
pevnom a bezpečnom. Preto má
byť v každom kostole zvyčajne iba
jeden svätostánok.89

XI. Obrazy vystavené na úctu
veriacich

278. Obrazy (i sochy) Pána, Panny
Márie a svätých sa podľa veľmi sta-
rej tradície Cirkvi oprávnene vysta-
vujú v chrámoch na úctu veriacich.
Treba však dbať o to, aby jednak
ich počet nebol priveľký, jednak
aby boli rozložené v určitom po-
riadku a aby neodvádzali pozor-
nosť veriacich od samej bohosluž-
by.90 Obraz jedného a toho istého
svätého môže byť len jeden. Vo
všeobecnosti pri výzdobe kostola
a jeho zariadenia, čo sa týka ob-
razov, treba dbať, aby napomáhali
nábožnost' celého spoločenstva.

XII. Všeobecná úprava posvätného
miesta

279. Výzdoba nech dodá kostolu
skôr vznešenú jednoduchosť ako
prepych. Pri výbere prvkov, ktoré
patria k výzdobe, treba dbať o
pravdivosť veci a usilovať sa
o to, aby ozdoba kostola slúžila
k výchove veriacich a prispela
k dôstojnosti celého posvätného
miesta.

“ Porov. Posv. kongr. obradov, inštr. Musicum racram, ;. marca 1967, č. 23: AAS 59 (r967) str. 307.
" Posv. kongr. obradov, inštr. Eucharish'mm mysterium, 25. mája r967, č. 53: AAS 59 (1967) str.
568. Rituale Romanum, De sacra Communione et de cultu mysterii eucharistici extra Missam, ed. typ.

1973» 5. 9»

" Porov. Posv. kongr. obradov, inštr. Eur/mrirlimm mystérium, 25. máj-\ 1967, č. 54: 11.4559 (1967)
568; inštr. Inter Oecumrniti, 26. sept. 1964, č. 95: AAS 56 (1964) str. 898.

"9 Porov. Posv. kongr. obradov, inštr. Eucharirlicum m_ynerr'um, 25. mája r967, č. 52: AAS 59 (1967)
str. 568; inštr. Inter Oerumeniti, 26. sept. r964, č. 95: AAS 56 (1964) str. 898; Posv. kongr. sviatosti, inštr.
NuIIa umquam (empora, 28. mája r938, č. 4: AAS 30 (1938) str. 199—200. Rituale Romanum, De sacra Com-
munione et de cultu mysu-rii eucharistici extra Missam, ed. typ. X973, 45. 10-11.

““ Porov. Druhý varik. koncil, konšt. o posv. liturgii Sacrasam'lum Comilium, &. (25.

VŠEOBECNÉ SMERNICE - MISÁL 63*

