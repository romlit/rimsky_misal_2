aby sme sa s ním večne mohli radovať,

keď sa zjaví vo svojej sláve.

Lebo on je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po Všetky veky vekov.

NAD OBETNYMI DARMI
Láskavý Otče,

prijmi spásonosnú obetu, ktorú ti prinášame

v deň spomienky na svätú Katarínu, *

a pomáhaj nám, —

aby sme podľa jej učenia a príkladu
vždy horlivejšie vzdávali vďaky
tebe, pravému Bohu.

Skrze Krista, nášho Pána.

SPEV NA PRIjĺh/IANIE

Ak žijeme vo svetle, ako Boh je vo svetle,
tvoríme spoločenstvo medzi sebou,

a krv Ježiša Krista, jeho Syna,

nás očisťuje od každého hriechu. A.Ieluja.

PO PRIJÍMANÍ

Dobrotivý Bože,

prijali sme nebeský pokrm,

ktorý zázračne udržoval

aj pozemský život svätej Kataríny; *
vrúcne ťa prosíme, —

aby nám prijímanie tejto sviatosti
zaistilo večný život.

Skrze Krista, nášho Pána.

(I Jn 1» 7)

Z9. APRÍLA 523

