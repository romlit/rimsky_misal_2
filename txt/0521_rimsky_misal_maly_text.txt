Kňaz: Pán s vami.

Ľud: 1 s duchom tvojím.

Kňaz: Hore srdcia.

Ľud: Máme ich u Pána.

Kňaz: Vzdávajme vďaky Pánovi, Bohu nášmu.

L'ud: Je to dôstojné a správne.

Je naozaj dôstojné a správne,

dobré a spásonosné

vzdávať vďaky vždy a Všade

tebe, svätý Otče,

skrze tvojho milovaného Syna Ježiša Krista.
On je tvoje Slovo,

skrze ktoré si Všetko stvoril,

jeho si nám poslal

za Spasiteľa a Vykupiteľa.

Mocou Ducha Svätého stal sa človekom
a narodil sa z Márie Panny.

Aby splnil tvoju vôl'u

a'získal ti ľud svätý,

rozpäl ruky na kríži, zomrel za nás,

a tak zlomil moc smrti a zjavil vzkriesenie.
Preto s anjelmi a so všetkými svätými
hlásame tvoju slávu

a jedným hlasom voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov.
Plné sú nebesia i zem tvojej slávy.

_ Hosanna na výsostiach!

Požehnaný, ktorý prichádza v mene Pánovom.
Hosanna na výsostiach!

DRUHÁ EUCHARISTICKÁ MODLITBA 421

