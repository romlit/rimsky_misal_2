Ked podáva prijímajúcemu kalich, povie:

Krv Kristova.

Prijímajúci odpovie:

Amen.

Ked sa dáva prijímanie namáčaním hostie, kňaz povie:
Telo a krv Kristova.
Prijímajúci odpovie:

Amen.

Ked kňaz prijima Kristovo telo, začne sa spev na prijímanie.

Po skončení prijímania kňaz, diakon alebo akolyta purifikuje paténu nad
kalichom a potom aj kalich.

Pri puriňkovaní kňaz potichu hovorí:

Pane, čo sme prijali ústami,

nech očistí a posvätí naše srdce,

a čo sme prijali pod spôsobom časných darov,
nech sa nám stane zárukou večného života.

Potom sa kňaz môže vrátiť k sedadlu. Možno zachovať chvíľu ticha alebo
recitovať nejaký d'akovný žalm, prípadne spievať primeranú pieseň, podľa
toho, čo sa uzná za vhodnejšie.

Potom kňaz, stojac pri sedadle alebo pri oltári, povie:

Modlime sa.

Kňaz a veriaci zotrvajú chvíľu v tichej modlitbe, ak tak neurobili po pri-
jímaní. Nato kňaz s rozopätými rukami prednesie modlitbu po prijímaní.

Na konci modlitby lud zvolá:
Amen.

OBRAD PRIJÍMANIA 455

