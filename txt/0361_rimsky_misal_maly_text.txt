SPEV NA PRIJÍMANIE (Porov. Rím 4, 25)

Kristus, náš Pán,
bol vydaný na smrť pre naše hriechy
a vstal z mŕtvych pre naše oslobodenie. Aleluja.

PO PRIJÍMANÍ

Všemohúci Bože, *

ustavičnou láskou ochraňuj svoj ľud,
ktorý si vykúpil smrťou svojho Syna, —-—
a daj, aby sme sa bez prestania tešili

z jeho slávneho zmŕtvychvstania.

Lebo on žije a kraľuje na veky vekov.

 

SOBOTA
po Šiestej veľkonočnej nedeli

 

ÚVODNY SPEV (Porov. r Pt z, 9)

Vykúpený Ind, ohlasuj slávne skutky Pána,
ktorý ťa z tmy povolal
do svojho obdivuhodného svetla. Aleluja.

MODLITBA DNA

Bože a Otče náš, *

tvoj Syn pred svojím nanebovstúpením
prisľúbil apoštolom Ducha Svätého; —
prosíme t'a,

udeľ aj nám dary tvojho Ducha,

ako oni dostali rozmanité dary

nebeskej múdrosti.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

SOBOTA PO ŠIESTE] NEDELI 261

