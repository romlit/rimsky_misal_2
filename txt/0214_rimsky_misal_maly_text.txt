MODLITBA DNA

Všemohúci Bože,

svojimi vznešenými sviatosťami obnovuješ svet; *
prosíme ťa, daj, aby sa Cirkev vzmáhala

týmito znakmi tvojej prítomnosti -

a nikdy nezostala bez tvojej otcovskej pomoci.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milosrdný Otče, *

prijmi naše dary a dopraj, —-

aby nás táto svätá obeta oslobodila
od pripútanosti k hriechu

a pomáhala nám v úsilí

o prehĺbenie kresťanského života.
Skrze Krista, nášho Pána.

Pieseň vďaky pôstna (str. 360 n.).

SPEV NA PRIJÍMANIE (Ez 56, 37)

Pán hovorí:

Vložím do vás svojho ducha a spôsobím,
že budete kráčať podľa mojich príkazov,
že budete zachovávať a plniť moje výroky.

PO PRIJÍMANÍ

Prosíme ťa, láskavý Bože, *

nech nás obnovujúca sila tejto sviatosti
ustavične oživuje a posväcuje, —

aby sme sa stali hodnými večného života.
Skrze Krista, nášho Pána.

114 PôSTNE OBDOBIE

