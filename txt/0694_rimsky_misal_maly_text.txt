veľkú lásku k evanjeliovej chudobe; *

na jej príhovor nám pomáhaj

nasledovať Ježiša Krista v duchu chudoby, —
aby sme si zaslúžili

naveky žit' s tebou v nebeskom kráľovstve.
Skrze nášho Pána Ježiša Krista, tvojho Syna...

13 . augusta

SV. PONCIÁNA, PÁPEŽA,
A HYPOLITA, KNAZA,
MUČENÍKOV

Spoločná omša mučeníkov (str. 706 n.)
alebo duchovných pastierov (str. 731 n.).

MODLITBA DNA

Pane a Bože náš, prosíme ťa, *

daj, aby obdivuhodná statočnosť svätých mučeníkov
Ponciána a Hypolita —

ustavične v nás rozmnožovala lásku k tebe

a upevňovala svätú vieru.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

15. augusta
NANEBOVZATIE PANNY MÁRIE

Slávnosť
Omša na vigíliu

Tento omšový formulár sa používa večer 14. augusta, a to pred prvými
vešperami slávnosti alebo po nich.

594 VLASTNÉ OMŠE svÁTýCH

