aby sme vždy lepšie poznávali nášho Spasiteľa

a žili jeho životom.

O to ťa prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
V jednote s Duchom Svätým po všetky veky vekov.

Po slávnosti Zjavenia Pána

Všemohúci Bože,

svetlom hviezdy si zjavil národom

príchod Spasiteľa sveta; *

prosíme t'a, —

daj, aby sme vždy dokonalejšie poznávali
tajomstvo narodenia tvojho Syna Ježiša Krista,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po Všetky veky vekov.

NAD OBETNYMI DARMI

Milosrdný Bože, *

prijmi obetné dary svojho ľudu, —

aby sme touto eucharistickou obetou
dosiahli dobrá,

po ktorých so synovskou dôverou túžime.
Skrze Krista, nášho Pána.

Pieseň vďaky pred slávnosťou Zjavenia Pána vianočná (str. 556 n.).
Po slávnosti Zjavenia Pána možno vziať pieseň vďaky o Zjavení Pána
(str. 359) alebo vianočnú (str. 356 n.).

SPEV NA PRIJÍMANIE (1 Jn 4, 9)

Božia láska k nám sa prejavila v tom,
že Boh poslal na svet svojho jednorodeného Syna,
aby sme skrze neho mali život.

PIATOK 65

