MODLITBA DNA

Pane a Bože náš,

ty otváraš bránu nebeského kráľovstva
všetkým, čo sa znovuzrodili

z vody a z Ducha Svätého; *

prosíme ťa, rozmnož v nás krstnú milosť, —
aby sme odolávali hriechu

a dosiahli nebeskú blaženosť,

ktorú si nám prisľúbil.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Láskavý Otče, *

prijmi dary plesajúcej Cirkvi,

ktorej si pripravil svätú veľkonočnú radosť, —
a priveď ju do večnej blaženosti.

Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočné (str. 367 n.).

SPEV NA PRIJÍMANIE (Rim 6, 8)

Veríme, že keď sme zomreli s Kristom,
spolu s ním budeme aj žiť. Aleluja.

PO PRIJÍMANÍ

Prosíme t'a, večný Bože, *
láskavo zhliadni na svoj ľud,
ktorý si duchovne obnovil

UTOROK PO TRETEJ NEDELI 227

