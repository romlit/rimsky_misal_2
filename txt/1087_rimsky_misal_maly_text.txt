NAD OBETNYMI DARMI

Bože, dobrotivý Otče,

zmiluj sa nad zosnulým diakonom M.,

Za ktorého ti prinášame túto spásonosnú obetu; *
pretože za života oddane slúžil tvojmu Synovi, —
daj mu odmenu verných služobníkov

v tvojom kráľovstve.

Skrze Krista, nášho Pána.

PO PRIJÍMANÍ

Milosrdný Otče, *

posilnení sviatostným pokrmom

prosíme t'a za tvojho služobníka, diakona M.,
ktorého si povolal k službe vo svojej Cirkvi; -—
vysloboď ho z moci smrti

a daj mu účasť na nebeskej hostine

v spoločenstve tvojich verných služobníkov.
Skrze Krista, nášho Pána.

;. ZA REHOĽNÍKA ALEBO REHOĽNÍČKU
MODLITBA

Všemohúci Bože,

tvoj služobník M. (tvoja služobnica M.)
miloval(a) Krista

a kráčal(a) cestou dokonalej lásky; *

prosíme ťa, —

nech sa raduje z tvojho slávneho príchodu

a nech žije uprostred svojich bratov (sestier)
vo večnej blaženosti nebeského kráľovstva.
Skrze nášho Pána Ježiša Krista, tvojho Syna...

ZA REHOĽNÍKA ALEBO REHOĽNÍČKU 987

