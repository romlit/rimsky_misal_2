Liturgia slova

131. Kým sa spieva Aleluja alebo
iný spev, ak je okiadzanie, poslu—
huje kňazovi pri vkladaní temianu
a potom sklonený pred kňazom
prosí o požehnanie, pričom po-
lohlasne hovorí Požchnaj ma, otče.
Kňaz ho požehnáva slovami Pán
nech je v tvojom srdci... Diakon
odpovie Amen. Potom vezme evan-
jeliár, ak je na oltári, a kráča k am-
bone. Pred nim idú — ak sú pri-
tomní — posluhujúci so svietnikmi
a prípadne s kadidlom. Tam po-
zdraví ľud, incenzuje knihu a ohla-
suje evanjelium. Po skončení po-
bozká knihu a ticho hovorí Slová
svätého evanjelia... Potom sa vráti
ku kňazovi. Ak nenasleduje homí-
lia ani vyznanie viery, môže zostať
na ambone na modlitbu veriacich,
zatiaľ čo posluhujúci odídu na
svoje miesta.

132. Úmysly modlitby veriacich
po kňazovom úvode prednáša sám
diakon z ambony alebo z iného
vhodného miesta.

Liturgia Eucharistie

133. Pri príprave obetných darov
kňaz zostane pri sedadle. Diakon
pripraví oltár za pomoci ostatných
posluhujúcich, pričom jemu patrí
starosť o posvätné nádoby. Asis-
tuje kňazovi aj pri preberaní da-
rov l'udu. Potom podá kňazovi
paténu s chlebom na konsekro-
vanie, naleje víno a trochu vody
do kalicha, pričom ticho hovorí
Tajomstvo tejto vody a podá ka-
lich kňazovi. Pripraviť kalich, čiže
naliať víno a vodu, môže však aj
pri stolíku. Ak sa používa kadidlo,
posluhuje kňazovi pri okiadzaní
obetných darov a oltára, a potom

sám alebo iný posluhujúci okiadza
kňaza i lud.

134. Počas eucharistickej modlitby
diakon stojí pri kňazovi, ale trochu
za ním, aby mu podľa potreby po-
slúžil pri kalichu alebo pri misáli.

135. Pri záverečnej doxológii eu-
charistickej modlitby stojí po boku
kňaza a drží zdvihnutý kalich, za—
tial' čo kňaz drží zdvihnutú paténu
s hostiou dovtedy, kým l'ud ne-
zvolá Amen.

r36. Keď kňaz predniesol modlitbu
pokoja a povedal Pokoj Pánov nech
je vždy s vami a keď ľud odpovedal
I s duchom tvojím, diakon môže
vyzvať lud na vzájomné prejavenie
pokoja slovami Dajte si znak po—
koja. On sám ho prijíma od kňaza
a môže ho dat' iným posluhujúcim,
ktorí sú mu nablízku.

137. Po kňazovom prijímaní aj on
prijíma pod obidvoma spôsobmi
a potom pomáha kňazovi pri po-
dávaní prijímania ludu. Keď sa
dáva prijímanie pod obidvoma
spôsobmi, on dáva prijímanie
z kalicha a posledný z kalicha
prijíma.

138. Po rozdaní prijímania diakon
sa vracia s kňazom k oltáru a po-
zbiera odrobinky, ak nejaké sú,
potom odnesie kalich a iné po-
svätné nádoby na stolík, tam ich
očistí a usporiada zvyčajným spô-
sobom. Kňaz sa medzitým vráti
k sedadlu. je dovolené ponechať na-
doby na očistenie na stolíku a očis-
tit' ich po omši, po prepustení
ludu. Majú byť však položené na
korporáli a vhodne prikryté.

Záverečné obrady

139. Po modlitbe po prijímaní dia-
kon, ak treba, vyhlási ludu krátke

VŠEOBECNÉ SMERNICE - MISÁL 45*

