NAD OBETNYMI DARMI

Bože a Otče náš,

svätého apoštola Petra si vnútorne osvietil,

aby vyznal vieru V teba, živého Boha,

a v tvojho Syna Ježiša Krista, *

a dal si mu silu,

aby slávnym mučeníctvom

vydal svedectvo o svojom Učiteľovi; —-

na jeho príhovor prijmi tieto obetné dary

a upevni nás v pravde a v apoštolskej horlivosti.
Skrze Krista, nášho Pána.

Pieseň vďaky o apoštoloch I. (str. 387).

SPEV NA PRIJÍMANIE (Mt 16, 16. x8)

Peter povedal Ježišovi:

Ty si Kristus, Syn živého Boha.

Pán Ježiš odpovedal:

Ty si Peter a na tejto skale postavím svoju Cirkev.

PO PRIJÍMANÍ

Láskavý Otče,

pri dnešnej spomienke na svätého Petra

dal si nám chlieb života; *

s vďačným srdcom ťa prosíme, udeľ nám milosť, —
aby sme verne nasledovali tvojho Syna,

ktorý jediný má slová večného života

a vedie nás ako dobrý pastier

do nebeského kráľovstva.

O to t'a prosíme skrze Krista, nášho Pána.

O SV. APOŠTOLOVI PETROVI 949

