SPEV NA PRIJÍMANIE (ju 15, 15)

Pán Ježiš hovorí: Už vás nenazývam sluhami,

lebo sluha nevie, čo robí jeho pán.

Nazval som vás priateľmi, pretože som vám oznámil všetko,
čo som počul od svojho Otca.

PO PRIJÍMANÍ

Všemohúci Bože,

na sviatok svätého M. a M.

s vďačným srdcom oslavujeme počiatky našej viery *
a chválime t'a,

že obdivuhodne pôsobíš vo svojich svätých; -
daj nech nás naplní radosťou spasiteľný dar,

ktorý sme prijali z tvojho oltára.

Skrze Krista, nášho Pána.

10.0mša o misionároch

ÚVODNY SPEV

Oslavujeme svätého M. a všetkých Božích priateľov,
ktorí sa preslávili hlásaním Božej pravdy.

MODLITBA DNA

Večný Bože,

misijnou činnosťou svätého (biskupa) M.
priviedol si mnohých ľudí

z temnôt k svetlu pravdy; *

na jeho príhovor nám pomáhaj, —

aby sme zostali pevní vo viere

a neochvejne sa pridŕžali nádeje,

ktorú nám dáva evanjelium.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

738 SPOLOČNÉ OMŠE

