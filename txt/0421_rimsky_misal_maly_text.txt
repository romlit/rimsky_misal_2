sviatostného pokrmu

napredovať v čnosti, —

aby sme správne užívali pozemské dary

a stále rástli v túžbe po nebeských dobrách.
Skrze Krista, nášho Pána.

 

TRIDSIATA NEDEĽA

 

ÚVODNý SPEV (ž 104, 3-4)

Nech sa radujú srdcia tých, čo Pána hľadajú.
Hľadajte Pána a jeho moc,
hľadajte vždy jeho tvár.

Oslavná pieseň.

MODLITBA DNA

Všemohúci a večný Bože,

rozmnož v nás vieru, nádej a lásku *

a pomáhaj nám milovať, čo prikaZuješ, —

aby sme dosiahli, čo sľubuješ.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

NAD OBETNýMI DARMI

Vznešený Bože, *

zhliadni na dary,

ktoré ti prinášame s oddaným srdcom, —
a daj, aby táto eucharistická obeta

slúžila na tvoju slávu.

Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná (str. 374 n.).

TRIDSIATA NEDEĽA 321

