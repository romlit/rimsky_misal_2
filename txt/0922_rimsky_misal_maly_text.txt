b) v druhej eucharistickej modlitbe po slovách a so všetkými kňazmi a diakonmi
sa dodá:

Pamätaj, Otče, aj na tieto naše sestry,

ktoré pre teba opustili všetko,

aby t'a našli vo všetkých ľuďoch, ktorým budú slúžiť,
a nezištne pomáhali všetkým trpiacim a núdznym.

c) v tretej eucharistickej modlitbe po slovách i všetok vykúpenj ľud sa dodá:

Láskavo posilňuj'vo svätom rozhodnutí
tieto svoje služobnice,

ktoré chcú oddane a dokonale
nasledovať Ježiša Krista

príkladným životom podľa evanjelia

a úprimnou sesterskou láskou.

d) vo štvrtej eucharistickej modlitbe po slovách na všetkých kňazov a diakonov
sa dodá:

i na tieto naše sestry,
ktoré sa ti dnes navždy zasvätili rehoľnými sľubmi,
na obetujúcich i tu prítomných,

SPEV NA PRIJÍMANIE (Gal 2, 19-20)

S Kristom som ukrižovaný.
A nežijem už ja, ale žije vo mne Kristus.

PO PRIJÍMANÍ

Nebeský Otče,

s nábožnou úctou sme prijali nebeský pokrm *
a prosíme t'a za tvojich služobníkov

(tvoje služobnice),

ktorí(é) sa ti zasvätili rehoľnými sľubmi; —
rozniet' v nich oheň Ducha Svätého

822 OMŠE PRI VYSLUHOVANÍ SVÁTENÍN

