Napokon bola vzatá do nebeskej slávy,

s materskou láskou sprevádza putujúcu Cirkev
a starostlivo ju chráni na ceste do večnej vlasti,
kým nenadíde preslávny deň Pánov.

Preto ťa so všetkými anjelmi a svätými oslavujeme
a bez prestania voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov...

SPEV NA PRIJÍMANIE (Porov. Jn 2, 1.11)

V Káne Galilejskej bola svadba. Bola tam aj ]ežišova matka.
Vtedy urobil Ježiš prvý zázrak a zjavil svoju moc.
A jeho učeníci uverili v neho.

Alebo: (Porov. Jn 19, 26-27)
Pán Ježiš, pribitý na kríži,
povedal učeníkovi, ktorého miloval: Hľa, tvoja matka.

PO PRIJÍMANÍ

Všemohúci Bože,

prijali sme záloh vykúpenia

a večného života; *

vrúcne ťa prosíme, —

daj, aby tvoja Cirkev

s materskou pomocou Panny Márie
ohlasovala evanjelium všetkým národom
a naplnila svet prejavmi Ducha Svätého.
Skrze Krista, nášho Pána.

C

0 MENE PREBLAHOSLAVENEJ
PANNY MÁRIE

Spoločná omša na sviatky Panny Márie (str. 695) podľa rozličných
období. Modlitba dňa je vlastná.

o PREBL. PANNE MÁRII 943

