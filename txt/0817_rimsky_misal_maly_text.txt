že za tvoje slovo a za vernosť Ježišov1
položili život; *

prosíme t'a, daj nám silu Ducha Svätého, —

aby sme aj my ochotne prijímali pravdy viery

a smelo ich vyznávali.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

Alebo:

Nekonečný Bože, ty dávaš svojim verným

stálosť vo viere a silu v slabosti; *

na príhovor mučeníkov M. a M. dopraj aj nám účasť
na smrti a zmŕtvychvstaní tvojho Syna, —

aby sme v spoločenstve s nimi

dosiahli dokonalú radosť u teba.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milosrdný a večný Bože, *

v deň víťaznej smrti tvojich svätých mučeníkov
slávime pamiatku obety tvojho Syna

a vyznávame, —

že každé mučeníctvo má pôvod a silu

v tejto jedinej obete Ježiša Krista,

ktorý s tebou žije a kraľuje na veky vekov.

SPEV NA PRIJÍMANIE (Ziv z, 7)

Kto zvíťazí, tomu dám jesť zo stromu života,
ktorý je v Božom raji. Aleluja.

svÁTýCH MUČENÍKOV 717

