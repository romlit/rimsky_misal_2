V.: Hore srdcia.

O.: Máme ich u Pána.

V.: Vzdávajme vd'aky Pánovi, Bohu nášmu.
O.: je to dôstojné a správne.

Je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože.
Lebo tvoja dobrota stvorila človeka

a povzniesla ho tak vysoko,

že zväzok muža a ženy

nám verne zobrazuje tvoju lásku.

Z lásky si človeka stvoril,

na cestu lásky ho neprestajne voláš,

aby si mu dal podiel na svojej večnej láske.
Tajomná sila sviatostného manželstva

ako znak tvojej lásky posväcuje ľudskú lásku
skrze nášho Pána Ježiša Krista.

Skrze neho s anjelmi a so všetkými svätými
spievame chválospev na tvoju slávu

a neprestajne voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov...

Ak sa berie Rímsky kánon, Bože, milostivo prijmi je vlastné. Slová, ktoré
sú v zátvorkách, môžu sa podľa okolností vynechať.

Bože, milostivo prijmi túto obetu,

ktorú ti predkladáme my, tvoji služobníci,

aj títo novomanželia M. a M. a celá tvoja rodina,
ktora ta za nich vrúcne prosí.

Tak, ako si im doprial dožit sa tejto posvätnej chvíle,
daj im dožiť sa

(radosti z detí, tvojho daru, a)

požehnanej staroby.

(Skrze nášho Pána Ježiša Krista. Amen.)

ZA ŽENÍCHA A NEVESTU 803

