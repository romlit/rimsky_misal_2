Po slávnosti Zjavenia Pána

Bože, svetlo všetkých národov, *

dožič celému svetu trvalý pokoj

a do našich sŕdc vlej žiarivé svetlo, —

ktorým si osvecoval mysle našich praotcov.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milovaný Bože,

pôvodca úprimnej nábožnosti a pokoja, *
dožič, aby sme ťa týmto obetným darom
dôstojne uctili —

a účasťou na sviatostnej hostine

dosiahli svornost' a jednotu zmýšľania.

Skrze Krista, nášho Pána.

Pieseň vďaky pred slávnosťou Zjavenia Pána vianočná (str. 356 n.).
Po slávnosti Zjavenia Pána možno vziať pieseň vďaky o Zjavení Pána
(str. 359) alebo vianočnú (str. 356 n.).

SPEV NA PRIJÍMANIE (x ]n 1, z)

Život, ktorý bol u Otca,
sa nám zjavil.

PO PRIJÍMANÍ

Milosrdný Bože,

ty mnohorakým spôsobom

vedieš a posilňuješ svoj ľud; *

otcovsky nás ochraňuj teraz i v budúcnosti -—
a daj, aby nás radosť z pozemských dobier
pobádala väčšmi sa usilovať o hodnoty večné.

Skrze Krista, nášho Pána.

62 VIANOČNÉ OBDOBIE

