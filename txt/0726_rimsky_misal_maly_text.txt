na svätých mučeníkov Kozmu a Damiána,

ktorých si voviedol do večnej slávy; *

prosíme ťa, _

sprevádzaj aj nás svojou prozreteľnosťou

na ceste k tebe.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milosrdný a večný Bože, *

v deň víťaznej smrti tvojich svätých mučeníkov
slávime pamiatku obety tvojho Syna -—

a vyznávame, že každé mučeníctvo má pôvod a silu
v tejto jedinej obete ]ežiša Krista,

ktorý s tebou žije a kraľuje na veky vekov.

PO PRIJÍMANÍ

Pane a Bože náš, *

v deň spomienky na svätých mučeníkov
Kozmu a Damiána

uštedril si nám pri tejto obete

dary svojej milosti; —

daj, aby v nás Oltárna sviatosť

upevnila vernosť k tebe

a priniesla nám pokoj a spásu.

Skrze Krista, nášho Pána.

626 VLASTNÉ OMŠE svÁTýCH

