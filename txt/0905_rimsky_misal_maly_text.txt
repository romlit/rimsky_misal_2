Nech sa zúčastňujú na bohoslužbách Cirkvi
a vo svete nech svedčia o tebe.

A po šťastnej starobe nech sa stretnú

so svojimi drahými

V nebeskom kráľovstve.

Skrze Krista, nášho Pána.

O.: Amen.

Omša pokračuje zvyčajným spôsobom.

SPEV NA PRIJÍMANIE (Ž 33, r- 9)

Pána chcem velebiť v každom čase,

moje ústa ho budú vždy chváliť.

Skúste a presvedčte sa, aký dobrý je Pán;
šťastný človek, čo sa utieka k nemu.

PO PRIJÍMANÍ

Všemohúci Bože, *

daj, aby sila prijatej sviatosti
mohutnela v týchto novomanželoch —
a my všetci aby sme získali
spasiteľný účinok z tejto obety.

Skrze Krista, nášho Pána.

POŽEHNANIE NA KONCI OMŠE

Pán Ježiš, ktorý sa zúčastnil na svadbe v Kane,
nech vám i vašim príbuzným udelí svoje pože-
hnanie.

O.: Amen.

Pán Ježiš, ktorý až do krajností miloval svoju Cirkev,
nech neprestajne vlieva do vašich sŕdc svoju lásku.
O.: Amen.

ZA ŽENÍCHA A NEVESTU 805

