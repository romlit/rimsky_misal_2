MODLITBA DNA

Bože a Otče náš,

ty si vnukol svätému opátovi Antonovi,

aby ti služil obdivuhodným životom na púšti; *
prosíme ťa, na jeho orodovanie dopraj aj nám, —
aby sme premáhali svoje sebectvo

a nadovšetko milovali teba.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Svätý Bože, nech sú ti príjemné naše dary,
ktoré pri spomienke na svätého Antona

s oddanosťou kladieme na oltár; *

pomáhaj nám, —

aby sme sa nepripútali k pozemským veciam
a svoje bohatstvo hľadali iba v tebe.

Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Mt 19, 21)

Pán Ježiš hovorí: Ak chceš byt' dokonalý, choď, predaj, čo máš,
rozdaj chudobným, a nasleduj ma.

PO PRIJÍMANÍ

Všemohúci Bože, ty si dal svätému Antonovi silu
víťaziť nad mocnosťami temností; *

prosíme ťa, posilní nás spasiteľnou sviatosťou,
ktorú sme prijali, —

aby sme aj my premáhali všetky nástrahy
nepriateľa našej spásy. Skrze Krista, nášho Pána.

17. ]ANUÁRA 485

