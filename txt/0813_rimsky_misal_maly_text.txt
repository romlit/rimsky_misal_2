_ SPEV NA PRIJÍMANIE (Porov. Rím s, 38-39)

Ani smrť, ani život, ani nijaké stvorenie
nás nebude môcť odlúčiť od lásky ku Kristovi.

PO PRIJÍMANÍ

Dobrotivý Otče,

pri oslave svätých mučeníkov M. a M.
posilnil si nás predrahým telom

a krvou svojho Syna; *

vrúcne ťa prosíme o vytrvalú lásku k tebe, —
aby sme žili v tvojej milosti

a odhodlane kráčali k tebe.

Skrze Krista, nášho Pána.

6.0mša jedného mučeníka
mimo veľkonočného obdobia

ÚVODNý SPEV

Oslavujeme svätca, ktorý za Boží zákon
položil svoj život

a nebál sa hrozby bezbožných,

lebo mal základy na pevnej skale.

MODLITBA DNA

Všemohúci a milosrdný Bože,

oslavujeme víťaznú smrť svätého mučeníka M.,
ktorému si dal silu pretrpiet' všetky muky; *
prosíme t'a, pomáhaj nám, —

aby sme aj my víťazne čelili všetkým

útokom nepriateľa.

svÁTýCH MUČENÍKOV 713

