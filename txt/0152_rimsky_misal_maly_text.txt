MODLITBA DNA

Všemohúci Bože,

pre zásluhy narodenia tvojho milovaného Syna *
vysloboď nás zo starého otroctva, —

ktoré nás drží v jarme hriechu.

O to t'a prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery sa vynechá.

NAD OBETNYMI DARMI

Milosrdný Bože, *

prijmi obetné dary svojho ľudu, _

aby sme touto eucharistickou obetou
dosiahli dobrá,

po ktorých so synovskou dôverou túžime.
Skrze Krista, nášho Pána.

Pieseň vďaky vianočná (str. 356 n.).
Ak sa recituje Rímsky kánon, použije sa vlastná vložka V spoločenstve.

SPEV NA PRIJÍMANIE (jn I, ró)

Z Kristovej plnosti sme prijali všetci milosť za milosťou.

PO PRIJÍMANÍ

Milosrdný Bože,

láskavo sa stretávaš s nami

V Najsvätejšej sviatosti; *

daj, aby jej sila

neprestajne účinkovala v našich srdciach —
a uspôsobila nás na prijatie ďalších milostí.
Skrze Krista, nášho Pána.

52 VIANOČNÉ OBDOBIE

