daj, aby sme sa pôsobením tvojej milosti

čoraz dokonalejšie premieňali v ježiša Krista, —

v ktorom je naša ľudská prirodzenosť

spojená s tvojím božstvom.

O to ťa prosíme skrze nášho Pána ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milovaný Bože,

pôvodca úprimnej nábožnosti a pokoja, *
dožič, aby sme t'a týmto obetným darom
dôstojne uctili —

a účasťou na sviatostnej hostine

dosiahli svornost' a jednotu zmýšľania.
Skrze Krista, nášho Pána.

Pieseň vďaky pred slávnosťou Zjavenia Pána vianočná (str. 356 n.).
Po slávnosti Zjavenia Pána možno vziať pieseň vďaky o Zjavení Pána
(str. 359) alebo vianočnú (str. 356 n.).

SPEV NA PRIJÍMANIE (Jn :, 16)

Z Kristovej plnosti sme prijali všetci
milosť za milosťou.

PO PRIJÍMANÍ

Milosrdný Bože,

ty mnohorakým spôsobom

vedieš a posilňuješ svoj ľud; *

otcovsky nás ochraňuj teraz i v budúcnosti —
a daj, aby nás radosť z pozemských dobier
pobádala väčšmi sa usilovať o hodnoty večné.
Skrze Krista, nášho Pána.

SOBOTA 67

