Preto t'a oslavujeme
a so všetkými anjelmi chválime
a radostne voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov

26. SPEV NA PRIJÍMANIE (Mt 26, 42)

Pán Ježiš sa modlil:

Otče môj, ak ma nemôže minúť tento kalich
bez toho, že by som z neho pil,

nech sa stane tvoja vôľa.

27 .- PO PRIJÍMANÍ

Dobrotivý Otče,

posilnil si nás presvätým chlebom z neba

a upevnil si nás vo viere a nádeji,

že pre smrť tvojho Syna získame večný život; *
prosíme t'a, --

dai, aby sme skrze jeho zmŕtvychvstanie
dosiahli cieľ nášho putovania.

Skrze Krista, nášho Pána.

KVETNÁ NEDEĽA 143

