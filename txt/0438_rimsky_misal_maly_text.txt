PO PRIJÍMANÍ

Nebeský Otče,

s radosťou sme prijali chlieb večného života

a sme šťastní, že môžeme poslúchať Ježiša Krista,
Kráľa neba i zeme; *

dožič nám, prosíme, —

aby sme s ním naveky žili v nebeskom kráľovstve.
O to t'a prosíme skrze Krista, nášho Pána.

338 KRISTA KRÁĽA

