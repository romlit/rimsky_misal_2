aby nám svojou smrťou zaslúžil nesmrteľnosť.

Lebo on žije a kraľuje na veky vekov.
Pieseň vďaky adventná II. (str. 555).

SPEV NA PRIJÍMANIE (Mt :, 23)

Dajú mu meno Emanuel,
čo v preklade znamená: Boh s nami.

PO PRIJÍMANÍ

Dobrotivý Otče, *

preukáž nám svoje milosrdenstvo

v tomto svätom chráme, —

aby sme sa účasťou na bohoslužbách

náležite pripravili na blízke sviatky nášho vykúpenia.
Skrze Krista, nášho Pána.

m_—

19. decembra
%

ÚVODNY SPEV (Porov. Hebr IO, 37)

Pán príde, nenechá na seba čakať.
A na našej zemi už nebude úzkosť, lebo on je náš Spasiteľ.

MODLITBA DNA

Večný Bože,

narodením tvojho Syna z presvätej Panny

zjavil si svetu jas svojej slávy; *

prosíme ťa, —

daj, aby sme toto nevýslovné tajomstvo vtelenia
uctievali so živou vierou

a oslavovali s úprimnou oddanosťou.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

19. DECEMBRA 31

