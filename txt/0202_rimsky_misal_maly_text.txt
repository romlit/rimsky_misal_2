 

TRETIA PČSTNA NEDEĽA

 

Keď sa v túto nedeľu konajú prípravné skúšky na krst dospelých,
možno použiť modlitby a prosby z omší pri udeľovaní sviatostí a svätenín

(str- 774)-

ÚVODNý SPEV (z 24, 15-16)

Stále upieram oči k Bohu o pomoc

a on ma vyslobodí z osídla.

Bože, zhliadni na mňa a zl'utuj sa nado mnou,
lebo som opustený a biedny.

Alebo: (Ez 36, 23-26)

Pán hovorí: Na vás ukážem, že som svätý;
zhromaždím vás zo všetkých krajín,
vylejem na vás vodu čistú,

a budete očistení od všetkej špiny;

a vložím do vás nového ducha.

Oslavná pieseň sa vynechá.

MODLITBA DNA

Bože, prameň nekonečného milosrdenstva

a nesmiernej dobroty, *

ty nás učíš,

že pôst, modlitba a skutky kresťanskej lásky
sú liekom proti hriechu; —

láskavo prijmi vyznanie našej slabosti,

a keď klesáme pod ťarchou previnení,
pozdvihni nás svojou milosrdnou rukou.
Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým

po všetky veky vekov.

Vyznanie viery.

102 PôSTNE OBDOBIE

