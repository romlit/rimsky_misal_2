PO PRIJÍMANÍ

Milosrdný Bože, sviatostné telo a krv tvojho Syna
pomáhali svätému biskupovi Vojtechovi

prekonať všetky utrpenia; *

prosíme ťa, —

nech aj nás posilňuje sväté prijímanie,

aby sme si zachovali vieru a lásku k tebe.

Sere Krista, nášho Pána.

24. apríla
SV. JURAJA, MUČENÍKA

Spoločná omša mučeníkov (str. 719).

MODLITBA DNA

Všemohúci Bože,

velebíme tvoju moc vo svätom jurajovi,

ktorý svojím utrpením nasledoval tvojho Syna; *
prosíme t'a, daj nám na jeho orodovanie silu, _
aby sme vždy Zvíťazili nad zlom

a každodenný kríž niesli

v spojení s Ježišom Kristom,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

SV. FIDELA ZO SIGMARINGENU,
KNAZA A MUČENÍKA

Spoločná omša mučeníkov (str. 719)
alebo duchovných pastierov (str. 731 n.).

24. APRÍLA 519

