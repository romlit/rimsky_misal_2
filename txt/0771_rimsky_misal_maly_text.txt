PO PRIJÍMANÍ

Dobrotivý Bože, *

nech táto sviatosť zapáli v nás oheň lásky,

akým za spásu duší horel svätý František, —

aby sme žili zodpovedne

svojmu kresťanskému povolaniu

a spolu s ním dosiahli odmenu

prisľúbenú svedomitým robotníkom v tvojej vinici.
Skrze Krista, nášho Pána.

4. decembra

SV. JÁNA DAMASCÉNSKEHO,
KNAZA A UČITEĽA CIRKVI

Spoločná omša duchovných pastierov (str. 731 n.)
alebo učiteľov Cirkvi (str. 743 n.).

MODLITBA DNA

Všemohúci Bože, vo svätom Jánovi Damascénskom
si dal Cirkvi vynikajúceho učiteľa; *

poskytuj nám na jeho príhovor stálu pomoc, —
aby pravá viera, ktorú tak presvedčivo hlásal,

bola nám vždy prameňom svetla a sily.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

6. decembra
SV. MIKULÁŠA, BISKUPA

Spoločná omša duchovných pastierov-biskupov (str. 728 n.).

6. DECEMBRA 671

