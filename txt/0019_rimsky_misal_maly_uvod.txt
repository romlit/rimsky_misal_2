 

 

VŠEOBECNÉ SMERNICE

RÍMSKEHO MISÁLA

PREDSLOV

I. Keď Kristus Pán chcel so svo-
jimi učeníkmi sláviť veľkonočnú
večeru, pri ktorej ustanovil obetu
svojho tela a krvi, rozkázal pri-
pravit veľké zariadené večeradlo
(Lk 22, 12). Tento príkaz Cirkev
vždy vzťahovala aj na seba, keď
určovala predpisy o duchovnej prí-
prave ľudu, o miestach, obradoch
a textoch na slávenie najsvätejšej
Eucharistie. Podobne i terajšie
predpisy,vypracované v intenciách
Druhého vatikánskeho všeobecné-
ho koncilu, ako aj nový misál,
ktorý bude odteraz používať Cir-
kev rímskeho obradu pri slávení
svätej omše, to sú nové dôkazy
starostlivosti Cirkvi, jej viery a
stálej lásky voči vznešene'mu eu-
charistickému tajomstvu a súčasne
dosvedčujú aj jej nepretržitú a sú-
vislú tradíciu, hoci sa zaviedli aj
niektoré nové prvky.

Dôkaz nezmenenej viery

2. Obetnú povahu svätej omše,
ktorú v súhlase so všeobecnou cir-
kevnou tradíciou slávnostne vyhlá-
sil Tridentský koncil,l znova zdô-
raznil aj Druhý vatikánsky koncil,
keď o omši vyriekol tieto vý-

znamné slová: << Náš Spasiteľ pri
Poslednej večeri ustanovil eucha-
ristickú obetu svojho tela a krvi,
aby ňou v priebehu vekov nepre-
stajne pokračoval v obete kríža,
kým on sám nepríde. Tak zveril
Cirkvi, svojej milovanej neveste,
pamiatku svojej smrti a svojho
zmŕtvychvstania. »2

Čo takto uči koncil, vyjadrujú
omšové texty. Veď v eucharistic-
kých modlitbách sa vhodne a pres-
ne vyjadruje náuka, ktorú jadrne
naznačuje táto myšlienka z Leo-
niánskeho sakramentára: <<Kedy-
koľvek slávime pamiatku tejto
obety, koná sa dielo nášho vykú-
penia. »3 Keď kňaz v týchto mod-
litbách koná spomienku (anamné-
Zu), obracia sak Bohu iv zastúpení
všetkého ludu, vzdáva Bohu vďaky
a prináša živú a svätú obetu, čiže
dar Cirkvi a obetu, ktorou Boh
sám chcel byt' uzmierený,4 a prosí,
aby Kristovo telo a krv boli obetou,
v ktorej má Otec zaľúbenie a celý
svet spásu.5

Tak v novom misáli pravidlo
modlitby Cirkvi zodpovedá odve-
ke'mu pravidlu viery. Tým sa nám
totiž prízvukuje, že obeta kríža a
jej sviatostné obnovenie vo svätej
omši, ktorú Kristus Pán ustanovil
pri Poslednej večeri a prikázal

' Trid. koncil. sesia XXII, l7. sept. 1562: DS 1738-1759.

* Druhý vatik. koncil, konšt. o posvätnej liturgii Xatromnctum Comrilium, č. 47; porov. dogm. konšt.
o Cirkvi Lumen gentium, čl. 3 a 28; dekrét o kňazskej službe a živote Presbylemrum ordínir, č. 2, 4 a 5.

“ Porov. Veronský sakramentár, vyd. Mohlberg, č. 93.

' Porov. Tretiu eucharistická modlitbu.
5 Porov. Štvrtú eucharistickú modlitbu.

VŠEOBECNÉ SMERNICE - MISÁL l9*

