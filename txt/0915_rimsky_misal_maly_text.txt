V eucharistických modlitbách spomienka na zasvätené panny sa môže
konať týmito formulami:

a) Ked sa berie Rímsky kánon, Bože, milostivo prijmi je vlastné:
Bože, milostivo prijmi túto obetu,

ktorú ti predkladáme my, tvoji služobníci,

i tieto tvoje služobnice v deň ich zasvätenia;
sprevádzaj naše sestry,

ktoré sa dnes z tvojej milosti

dokonalejšie pridružili k tvojmu Synovi,

aby mu s radosťou mohli vyjsť v ústrety,
keď príde v sláve na konci vekov.

(Skrze nášho Pána Ježiša Krista. Amen.)

b) v druhej eucharistickej modlitbe po slovách a so všetkými kňazmi a
diakomni sa dodá:

Pamätaj, Otče, aj na tieto naše sestry,

ktoré si dnes posvätil duchovným pomazaním;

daj im milosť, aby s horiacimi lampami viery a lásky
neúnavne slúžili tebe i tvojmu ľudu

a očakávali príchod Ženícha, Ježiša Krista.

e) v tretej eucharistickej modlitbe po slovách i všetok vykúpený ľud sa dodá:

Láskavo posilňuj vo svätom rozhodnutí

tieto svoje služobnice, ktoré chcú oddane a dokonale
nasledovať Ježiša Krista

príkladným životom podľa evanjelia

a úprimnou sesterskou láskou.

d) vo štvrtej eucharistickej modlitbe po slovách na všetkých kňazov a
diakonov sa dodá:

i na tieto naše sestry, ktoré sa dnes navždy zasvätili
tvojej oslave a službe ľuďom,
na obetujúcich i tu prítomných.

PRI ZASVÁTENÍ PANIEN 815

