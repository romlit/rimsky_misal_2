daj, nech nás sila Ducha Svätého,

ktorá pôsobí vo sviatostiach, ustavične obnovuje,
aby sme ti zostali úprimne oddaní.

Skrze Krista, nášho Pána.

 

TRIDSIATA TRETIA NEDEĽA

 

ÚVODNY SPEV (]er 29, 11.12.14)

Pán hovori:

Mojím úmyslom je priniesť vám pokoj, a nie trápenie;
budete ma vzývat' a ja vás vyslyším

a vyslobodim vás zo všetkých miest vášho zajatia.

Oslavná pieseň.

MODLITBA DNA

Pane a Bože náš, *

pomáhaj nám,

aby sme t'a radostne a oddane uctievali, —

lebo trvalé a dokonalé šťastie dosiahneme iba vtedy,
keď budeme vždy slúžiť tebe,

pôvodcovi všetkého dobra.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

NAD OBETNYMI DARMI
Láskavý Otče, *

daj, aby nám obeta, ktorú slávime,
pomáhala žiť pre teba —

a zaistila nám večnú blaženosť.
Skrze Krista, nášho Pána.

TRIDSIATA TRETIA NEDEĽA 325

