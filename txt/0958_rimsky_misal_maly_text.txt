pre túto svätú obetu daj mi silu

žiť podľa tvojej vôle a hlásať tvoju pravdu, —
aby som slovom i príkladom

ukazoval veriacim cestu k tebe.

Skrze Krista, nášho Pána.

B
ÚVODNY SPEV (Ž 15, 2. ;)

Hovorím Pánovi: Ty si môj Pán. Pre mňa niet šťastia bez teba.
Ty, Pane, si môj podiel na dedičstve a na kalichu,
v tvojich rukách je môj osud.

MODLITBA DNA

Bože, milosrdný Otče, *

vypočuj moju modlitbu

a osvieť mi srdce milost'ou Ducha Svätého, —
aby som dôstojne slávil tvoje tajomstvá,
verne slúžil tvojej Cirkvi

a teba miloval vytrvalou láskou.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

NAD OBETNYMI DARMI

Všemohúci Bože,

prijmi obetné dary, ktoré ti s úctou prinášame, *
a pre svojho Syna Ježiša Krista,

ktorý je zároveň kňazom i obeťou,

zhliadni na mňa, svojho služobníka,

ktorému si dal podiel na jeho kňazstve,

a udeľ mi milosť, —

aby som ti stále obetoval aj seba samého

ako duchovnú obetu.
Skrze Krista, nášho Pána.

858 OMŠE ZA ROZLIČNÉ POTREBY

