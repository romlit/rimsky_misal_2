aj týchto svojich služobníkov,

poznačených jeho krížom

a duchovným pomazaním, —

aby sa ti s ním vždy obetovali,

a tak si čoraz viac zaslúžili dary tvojho Ducha.
Skrze Krista, nášho Pána.

Ak sa berie Rímsky kánon, Bože, milostivo prijmi je vlastné (pozri str. 784).

SPEV NA PRIJÍMANIE (Ž 53, 6.9)

Pristúpte k Pánovi a on vás osvieti,
skúste a presvedčte sa, aký je dobrý.

PO PRIJÍMANÍ

Bože, ty si svojich služobníkov

zahrnul darmi Svätého Ducha

a posilnil telom a krvou svojho Syna; *
prosíme t'a,

nauč ich láske, ktorá je splnením zákona, —-
aby svetu stále ohlasovali slobodu tvojich detí
a svätosťou života vykonávali

prorocké poslanie tvojho ľudu.

Skrze Krista, nášho Pána.

Slávnostné požehnanie alebo modlitba nad ľudom ako na str. 785 n.

C
INÉ MODLITBY

MODLITBA DNA

Prosíme t'a, Bože,
nech Obhajca Duch Svätý, ktorý vychádza z teba, *
osvecuje naše mysle a nech nás učí všetku pravdu, —-

PRI UDEĽOVANÍ BIRMOVKY 787

