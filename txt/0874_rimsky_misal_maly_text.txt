NAD OBETNYMI DARMI

Všemohúci a večný Bože, *

ty nás povolávaš vyznávať tvoje meno

a sviatosťou krstu nám dávaš život; -—

prijmi naše modlitby a obetné dary,

splň túžby uchádzačov o krst, ktorí dúfajú v teba,
a osloboď ich od hriechov.

Skrze Krista, nášho Pána.

Pieseň vďaky z príslušného liturgického obdobia.

SPEV NA PRIJÍMANIE (Ef 1, 7)

V Kristovi máme vykúpenie skrze jeho krv,
odpustenie hriechov podľa bohatstva jeho milosti.

PO PRIJÍMANÍ

Pane a Bože náš, *

sviatosť, ktorú sme prijali,

nech nás oslobodí od všetkých hriechov; -—
a keď nás obžalúva svedomie,

príď nám na pomoc svojou milosťou.
Skrze Krista, nášho Pána.

Možno vziať aj omšu z piatku po Štvrtej pôstnej nedeli (str. 118).

z. PRI PRÍPRAVNYCH SKÚŠKACH NA KRST

Táto omša sa berie pri prípravných skúškach na krst, a to alebo v ča-
se pre ne určenom, to jest na Tretiu, Stvrtú a Piatu pôstnu nedelu, alebo
inokedy.

ÚVODNY SPEV (Ez 36, 23-26)

Pán hovorí: Na vás ukážem, že som svätý:
zhromaždím vás zo všetkých krajín,
vylejem na vás vodu čistú,

774 OMŠE PRI VYSLUHOVANÍ SVIATOSTÍ

