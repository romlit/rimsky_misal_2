aby nás chránili od hriešnej nezriadenosti —
a posilňovali na ceste k večnej spáse.
Skrze Krista, nášho Pána.

Pieseň vďaky pôstna (str. 360 n.).

SPEV NA PRIJÍMANIE (Lk 15, 32)

Syn môj, treba sa radovať,
lebo tvoj brat bol mŕtvy, a ožil,
bol stratený, a našiel sa.

PO PRIJÍMANÍ

Láskavý Bože,

vznešená sviatosť, ktorú sme nábožne prijali, *
nech prenikne naše srdcia, —

aby sme vždy horlivejšie žili pre teba.

Skrze Krista, nášho Pána.

SOBOTA PO DRUHÉ] NEDELI 101

