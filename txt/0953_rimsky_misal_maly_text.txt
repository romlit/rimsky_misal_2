a pre obetu, ktorú ti s radosťou prinášame, —
daruj svätej Cirkvi (daruj našej diecéze)
pastiera podľa svojho 'srdca.

Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (]n I;, 16)

]a som si vás vyvolil a ustanovil som vás,
aby ste šli a prinášali ovocie a vaše ovocie aby zostalo.

PO PRIJÍMANÍ

Dobrotivý Otče,

posilnil si nás sviatosťou tela a krvi tvojho Syna; *
milostivo nám daj takého pastiera, -—

ktorý by tvojmu ľudu

horlivo ohlasoval pravdu evanjelia

a viedol ho k čnostnému životu.

Skrze Krista, nášho Pána.

5. ZA KONCIL ALEBO SYNODU

ÚVODNY SPEV (Kol 3, 14-15)

Nadovšetko Zaodejte sa láskou. Ona je dokonalým spojivom.
Vo vašich srdciach nech kraľuje pokoj Kristov.

MODLITBA DNA

Bože, správca a ochranca svojej Cirkvi, *

daj všetkým účastníkom koncilu (synody)

ducha múdrosti, pravdy a pokoja, -

aby s úprimným srdcom hľadali, čo sa tebe páči,
a s vytrvalým úsilím uskutočňovali tvoju vôľu.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

ZA KONCIL ALEBO SYNODU 853

