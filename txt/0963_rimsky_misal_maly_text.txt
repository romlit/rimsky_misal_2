SPEV NA PRIJÍMANIE (1 Jn ;, 16)

V tom sme poznali Božiu lásku,
že Kristus svoj život položil za nás.
Aj my máme položiť život za bratov.

PO PRIJÍMANÍ

Všemohúci Bože,

pri sviatostnom stole posilnil si nás chlebom života ; *
prosíme ťa, nech účinkom tejto sviatosti lásky
dozrievajú semená povolaní,

ktoré štedro rozsievaš na roli svojej Cirkvi, —

aby si mnohí vyvolili za svoje životné povolanie
slúžiť tebe vo svojich bratoch.

Skrze Krista, nášho Pána.

IO. ZA REHOĽNÍKOV

ÚVODNY SPEV (Ž 36, 3-4)

Spoľahni sa na Pána a dobre rob,

a budeš bývať vo svojej krajine a tešiť sa z bezpečia.
Hladaj radosť v Pánovi,

a dá ti, za čím túži tvoje srdce.

MODLITBA DNA

Všemohúci Bože,

ty vnukáš dobré úmysly

a pomáhaš ich aj uskutočniť; *

prosíme ťa za našich bratov a sestry,

ktorí zanechali všetko a úplne sa zasvätili tebe,

ZA REHOĽNÍKOV 863

