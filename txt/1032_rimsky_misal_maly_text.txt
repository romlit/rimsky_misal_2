SPEV NA PRIJÍMANIE (Sk 4, n)

Pod nebom niet iného mena, daného ľuďom,
v ktorom by sme mohli byť spasení.

PO PRIJÍMANÍ

Všemohúci Bože, *

ty si ustanovil, aby na meno Ježiš
pokl'aklo každé koleno

a aby v ňom všetci ľudia našli spásu; —
daj nám milosť

klaňať sa so živou vierou Pánu Ježišovi
prítomnému v Oltárnej sviatosti.

Lebo on žije a kraľuje na veky vekov.

5. o PREDRAHE] KRVI
NÁŠHO PÁNA JEŽIŠA KRISTA

Táto omša sa slávi v červenom rúchu.

ÚVODNY SPEV (Ziv 5, 9-10)

Vykúpil si nás svojou krvou, Pane,
z každého kmeňa, jazyka, ľudu a národa
a urobil si z nás kráľovstvo nášmu Bohu.

MODLITBA DNA

Milosrdný Bože,

ty si vykúpil všetkých ľudí

drahocennou krvou svojho Syna; *

zachovaj v nás dielo svojho milosrdenstva, —
aby sme slávením týchto svätých tajomstiev
získali vždy hojnejšie ovocie nášho vykúpenia.
Skrze nášho Pána Ježiša Krista, tvojho Syna...

932 VOTÍVNE OMŠE

