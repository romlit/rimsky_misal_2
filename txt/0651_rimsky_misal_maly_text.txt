PO PRIJÍMANÍ

Dobrotivý Otče,

nasýtil si nás nebeským pokrmom; *

daj, nech nás svätý Ján Krstiteľ

sprevádza svojím orodovaním —

a nech nám vyprosí zmilovanie u tvojho Syna,
ktorého nazval Baránkom, čo sníma hriechy sveta.
O to ťa prosíme skrze Krista, nášho Pána.

Omša v deň slávnosti

ÚVODNY' SPEV (]n :, 6-7; Lk 1, 17)

Narodil sa človek, ktorého poslal Boh.
Volal sa Ján.

Prišiel ako svedok svedčiť o Svetle

a pripraviť Pánovi dokonalý l'ud.

Oslavná pieseň.

MODLITBA DNA

Dobrotivý Bože,

ty si poslal svätého Jána Krstiteľa,

aby pripravil starozákonný ľud

na príchod Ježiša Krista; *

daruj svojej Cirkvi radosť v Duchu Svätom _

a veď nás stále po ceste pokoja a spásy.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

24. JÚNA 551

