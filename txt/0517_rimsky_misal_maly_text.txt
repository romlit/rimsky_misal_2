Jána, Štefana, Mateja, Barnabáša,
(Ignáca, Alexandra, Marcelína, Petra,
Felicity, Perpetuy, Agaty, Lucie,
Agnesy, Cecílie, Anastázie)

a všetkých tvojich svätých.

Prosíme ťa,

prijmi nás do ich spoločenstva,

nie pre naše zásluhy,

ale pre tvoje veľké zľutovanie.

Zopne ruky

Skrze nášho Pána Ježiša Krista.
Pokračuje:

Skrze" neho ty, Bože,

Všetky tieto dary stále tvoríš,
posväcuješ, oživuješ,
požehnávaš a nám dávaš.

Vezme paténu s hostiou a kalich, pozdvihne ich a hovorí:

Skrze Krista, s Kristom a v Kristovi
máš ty, Bože Otče všemohúci,

v jednote s Duchom Svätým

všetku úctu a slávu

po všetky veky vekov.

Ľud zvolá:

Amen.

PRVÁ EUCHARISTICKÁ MODLITBA 417

14

