 

 

VEĽKONOČNÁ NEDEĽA

 

OMŠA VO DNE

ÚVODNY SPEV (Porov. Lk 24, 34; Zjv x, 6)

Pán naozaj vstal z mŕtvych, aleluja.
jemu patrí sláva a moc na večné veky!

Alebo: (Porov. Ž 138, 18. 5-6)

Vstal som z mŕtvych a znova som s tebou,
viedol si ma svojou rukou.
Aká obdivuhodná je pre mňa tvoja múdrosť! Aleluja.

Oslavná pieseň.

MODLITBA DNA

Všemohúci a večný Bože,

ty si nám dnešného dňa

Kristovým víťazstvom nad smrťou

otvoril bránu do večnosti; *

preto s radosťou slávime jeho zmŕtvychvstanie
a prosíme ťa, _

obnov nás Duchom Svätým,

aby sme povstali k novému životu.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

NAD OBETNYMI DARMI

Milostivý Bože, *
s veľkonočnou radosťou
prinášame ti dary na obetu, —

VEĽKONOČNÁ NEDEĽA 205

