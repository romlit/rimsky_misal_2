Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje
V jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Pane a Bože náš, *

milostivo posvät' tieto obetné dary

a s nimi prijmi aj našu duchovnú obetu, -—
aby sme tvojou láskou milovali všetkých ľudí.
Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (I Kor 13, 13)

Teraz zostáva viera, nádej a láska, tieto tri,
no najväčšia z nich je láska.

PO PRIJÍMANÍ

Dobrotivý Otče, *

nasýti] si nás z toho istého Chleba,

ktorý v nás rozmnožuje božský život; —

naplň nás milosťou Ducha Svätého

a čistou radosťou, prameniacou z dokonalej lásky.
Skrze Krista, nášho Pána.

42. ZA SVORNOS'Í'

ÚVODNY SPEV (Sk 4, 32- 33)

Množstvo veriacich malo jedno srdce a jednu dušu.
Apoštoli s veľkou silou vydávali svedectvo

o zmŕtvychvstaní Pána ]ežiša

a všetci sa tešili veľkej obľube. Aleluja.

ZA SVORNOST 919

