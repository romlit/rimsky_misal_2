Nech dobrotivo stojí pri vás v šťastí i v nešťastí
jednorodený Syn Boží.
O.: Amen.

Nech do vašich sŕdc vlieva svoju lásku Duch Svätý.
O.: Amen.

Aj vás všetkých tu prítomných nech žehná vše-
mohúci Boh, Otec i Syn % i Duch Svätý.
O.: Amen.

B
ÚVODNY SPEV (ž 89, 14.17)

Hneď zrána nás naplň, Pane, svojou milosťou

a budeme jasať a radovať sa po všetky dni života.
Nech je nad nami dobrotivosť Pána, nášho Boha;
upevňuj dielo našich rúk.

MODLITBA DNA

Bože, vyslyš naše pokorné prosby *

za týchto tvojich služobníkov M. a M.,

ktorí pri tvojom oltári spájajú svoje životy; —
naplň ich milosťou a upevni vzájomnou láskou.
Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Bože, láskavo prijmi dary, *

ktoré ti v túto radostnú chvíľu prinášame, —
a s otcovskou láskou ochraňuj tých,

ktorých si spojil sviatostným zväzkom.

Skrze Krista, nášho Pána.

ZA ŽENÍCHA A NEVESTU 797

