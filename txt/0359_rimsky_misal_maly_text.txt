NAD OBETNýMI DARMI

Nebeský Otče, prijmi naše modlitby,

ktorými sprevádzame tieto obetné dary, *

a láskavo očisti naše srdcia, ——

aby sme mohli dôstojne sláviť sviatosť tvojej lásky.
Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočné. (str. 367 n.).

SPEV NA PRIJÍMANIE (Mt 28, zo)

Hľa, ja som s vami po všetky dni,
až do skončenia sveta. Aleluja.

PO PRIJÍMANÍ

Všemohúci a večný Bože,

zmŕtvychvstaním tvojho Syna

znovu si nám otvoril cestu do večného života; *
Zveľaďuj v nás účinky veľkonočného tajomstva —
a naplň nás silou sviatostného pokrmu,

ktorý sme prijali.

Skrze Krista, nášho Pána.

PIATOK
_ po Šiestej veľkonočnej nedeli

ÚVODNY SPEV (Ziv 5, 9-10)

Vykúpil si nás svojou krvou, Pane,

z každého kmeňa, jazyka, ľudu a národa
a urobil si nás nášmu Bohu

kráľovským kňazstvom. Aleluja.

FIAT-OK PO ŠIESTEJ NEDELI 259

