Verím v Ducha Svätého,

Pána a Oživovateľa,

ktorý vychádza z Otca i Syna.

Jemu sa zároveň vzdáva

tá istá poklona a sláva ako Otcovi a Synovi.
On hovoril ústami prorokov.

Verím v jednu, svätú, všeobecnú, apoštolskú Cirkev.
Vyznávam jeden krst na odpustenie hriechov

a očakávam vzkriesenie mŕtvych

a život budúceho veku. Amen.

Namiesto veľkého vyznania viery možno recitovať Apoštolské vyznanie
viery.

APOŠTOLSKÉ VYZNANIE VIERY

Verím v Boha,

Otca všemohúceho, Stvoriteľa neba i zeme,
i v Ježiša Krista,

jeho jediného Syna, nášho Pána,

Pri slovách ktorý sa počal narodil sa z .Máríe Panny sa všetci uklonia.

ktorý sa počal z Ducha Svätého,

narodil sa z Márie Panny,

trpel za vlády Poncia Piláta,

bol ukrižovaný, umrel a bol pochovaný;
zostúpil k zosnulým,

tretieho dňa vstal z mŕtvych,

vstúpil na nebesia,

sedí po pravici Boha Otca všemohúceho.
Odtiaľ príde súdiť živých i mŕtvych.

LITURGIA SLOVA 349

