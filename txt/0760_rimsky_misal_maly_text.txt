O to ťa prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Dobrotivý Bože, posväť tieto dary,
ktoré ti s radosťou prinášame

pri spomienke na svätého Martina; *
nech nás táto obeta posilní, --

aby sme v šťastí i v protivenstvách
kráčali správnou cestou.

Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Mt 25, 40)

Pán Ježiš povedal:
Čo ste urobili jednému z týchto mojich najmenších bratov,
mne ste urobili.

PO PRIJÍMANÍ

Pane a Bože náš,

posilnil si nás sviatosťou jednoty; *

pomáhaj nám ochotne plniť tvoju vôl'u

a úplne sa ti odovzdať ako svätý Martin, _
aby sme sa mohli tešiť, že patríme celkom tebe.
Skrze Krista, nášho Pána.

I z . novembra

SV. ]OZAFÁTA, BISKUPA A MUČENÍKA

Spomienka

Spoločná omša mučeníkov (str. 713 11.)
alebo duchovných pastierov-biskupov (str. 728 n.).

660 VLASTNÉ OMŠE SVÁTYCH

