MODLITBA DNA

Milosrdný Bože,

ty si dal svätej Margite nazrieť

do tajomstva lásky tvojho Syna,

ktorá prevyšuje všetko poznanie; *

prosíme ťa, aj nám daruj svojho Ducha, —

aby sme poznali Kristovu lásku

a mali účasť na bohatstve tvojho božského života.
Skrze nášho Pána Ježiša Krista, tvojho Syna...

17. októbra

sv. IGNÁCA ANTIOCHIJSKÉHO,
BISKUPA A MUČENÍKA

Spomienka

ÚVODNY' SPEV (Gal 2, 19-20)

S Kristom som ukrižovaný.

A nežijem už ja, ale žije vo mne Kristus.

No kým ešte žijem v tele, žijem vo viere v Syna Božieho,
ktorý si ma zamiloval a seba samého obetoval za mňa.

MODLITBA DNA

Všemohúci a večný Bože,

svedectvom svätých mučeníkov

ozdobuješ svoju Cirkev, Kristovo tajomné telo; *
daj, aby mučenícka smrť svätého Ignáca,

ktorá jeho priviedla do večnej slávy, —-

pre nás bola ustavičným zdrojom sily a odvahy
vo vyznávaní viery.

17 . OKTOBRA 641
21

