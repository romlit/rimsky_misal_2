a odovzdávajú ti dieťa,

ktoré si im požehnal; —

daj, aby sa raz mohli s ním radostne objať
v nebeskom kráľovstve.

Skrze Krista, nášho Pána.

Pieseň vďaky o zosnulých (str. 399 n.).

SPEV NA PRIJÍMANIE (Porov. Rím 6, 4.8)

Krstným ponorením do Kristovej smrti
sme boli pochovani s Kristom;

veríme, že spolu s Kristom budeme aj žiť.
(Aleluja).

PO PRIJÍMANÍ

Dobrotivý Otče,

pri sviatostnom stole sme prijali

telo a krv tvojho Syna *

a s dôverou ťa prosíme, —

aby nám táto sviatosť bola útechou

v zármutkoch tohto života

a upevnila nás v nádeji na život večný.
Skrze Krista, nášho Pána.

INÉ MODLITBY

MODLITBA DNA

Milosrdný Otče,

ty poznáš hlboký zármutok nášho srdca
nad smrťou tohto dieťaťa; *

prosíme ťa, —

1002 PRI POHREBE POKRSTENÉHO DIE'Í'A'Í'A

