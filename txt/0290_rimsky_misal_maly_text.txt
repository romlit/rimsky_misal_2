Prijmi ju so záľubou

a jej žiara nech sa spojí

so svetlom nebeských hviezd.

Nech ju nájde horieť ranná zornica,

tá zornica, ktorá nikdy nezapadá:

tvoj Syn ]ežiš Kristus,

ktorý slávne vstal z mŕtvych,

osvecuje ľudstvo veľkonočným svetlom

a s tebou žije a kraľuje na veky vekov.
Ľud: Amen. "

II. časť

Liturgia slova

zo. Na túto vigíliu, najdôležitejšiu zo všetkých vigílií, je určených deväť
čítaní, a to sedem zo Starého zákona a dve (epištola a evanjelium) z Nového
zákona.

21. Ak to vyžadujú pastoračné dôvody, možno vynechať niektoré čítania
zo Starého zákona. Treba však vždy pamätať na to, že čítanie Božieho
slova je podstatnou zložkou tejto vigílie. Majú sa čítať aspoň tri čítania
zo Starého zákona, v krajnom prípade najmenej dve. Nikdy sa však nesmie
vynechať čítanie z Druhej knihy Mojžišovej (14, 15 - 15, 1, tretie čítanie).

zz. Všetci odložia sviece a sadnú si. Pred čítaním kňaz povzbudí l'ud
týmito alebo podobnými slovami;

Drahí bratia a sestry, po slávnostnom úvode veľko-
nočnej Vigílie teraz pozorne počúvajme Božie slovo.
Uvažujme, ako Boh ochraňoval svoj ľud v minulosti
a ako napokon poslal svojho Syna ako Vykupiteľa.
Modlime sa, aby Boh dovŕšil v nás dielo vykúpenia,
ktoré Kristus uskutočnil svojou smrťou a zmŕtvych-
vstaním.

190 VEĽKON OČNÁ VIGÍLIA

