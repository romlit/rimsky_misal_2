PIESEN VĎAKY

O Kristovi - Kráľovi vesmíru

.: Pán s vami.

: I s duchom tvojím.

.: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

.O.<.0<.0<

]e naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože.
Lebo ty si svojho jednorodeného Syna,

nášho Pána Ježiša Krista, pomazal olejom veleby
za večného kňaza a kráľa všetkých svetov,

aby na oltári kríža obetoval sám seba

ako zmiernu obetu bez poškvrny,

a tak zavŕšil tajomstvá vykúpenia ľudstva,

aby podrobil svojej vláde celé stvorenie

a odovzdal ho tvojej nesmiernej velebnosti

ako večné a vesmírne kráľovstvo;

kráľovstvo pravdy a života,

kráľovstvo svätosti a milosti,

kráľovstvo spravodlivosti, lásky a pokoja.

Preto s anjelmi, archanjelmi

a so zástupmi všetkých svätých
spievame chválospev na tvoju slávu
a neprestajne voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov...

SPEV NA PRIJÍMANIE (Ž 28, 10-11)
Pán tróni ako večný král,
požehná svoj l'ud pokojom.

KRISTA KRÁĽA 337

