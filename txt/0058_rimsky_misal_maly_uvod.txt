pozor, aby z Krvi nič neodkvaplo,
odpije trochu z kalicha a odíde.
Posluhujúci utrie vonkajšiu časť
kalicha puriňkatóriom.

e) Ak sú takí, čo budú prijímať
len pod jedným spôsobom, poslu—
hujúci potom, keď už z kalicha
pili všetci, čo mali prijímať pod
obidvoma spôsobmi, odloží kalich
na oltár. Kňaz dáva prijímanie ve-
riacim a vráti sa k oltáru. Čo zo-
stalo 2 Krvi, prijima kňaz alebo
posluhujúci a vykoná purifikáciu
zvyčajným spôsobom.

245. Ak niet diakona ani iného
asistujúceho kňaza ani akolytu:

a) Kňaz prijíma ako zvyčajne
Pánovo telo a krv, pričom dbá,
aby v kalichu ostalo dosť Krvi pre
prijímajúcich; vonkajšiu časť ka-
licha utrie purifikatóriom.

b) Potom sa kňaz postaví tak,
aby mohol pohodlne udeľovať pri-
jímanie a zvyčajným spôsobom
podáva Pánovo telo každému, čo
bude prijímať pod obidvoma spô-
sobmi. Prijímajúci pristupujú, pre-
javia náležitú úctu a postavia sa
pred kňaza. Po prijatí Pánovho
tela trošku ustúpia.

0) Keď všetci prijali Pánovo telo,
kňaz položí cibórium na oltár a
vezme kalich s puriňkatóriom.
Všetci, čo majú prijímať z kalicha,
znova prídu ku kňazovi a postavia
sa pred neho. Kňaz hovorí Krv
Kristova a prijímajúci odpovie
Amen. A kňaz mu podá kalich s pu-
rifikatóriom. Prijímajúci drží la-
vou rukou puriftkatórium pod ústa-
mi a dáva pozor, aby z Krvi nič
neodkvaplo, odpije trochu z kali-
cha a odíde. Kňaz utrie vonkajšiu
časť kalicha purií-ikatóriom.

d) Po prijímaní kňaz položí ka-
lich na oltár a ak sú takí, čo majú

prijímať len pod jedným spôso-
bom, dáva im prijímanie ako ob—
vykle. Potom sa vráti k oltáru,
prijme, čo ešte zostalo z Krvi, a
vykoná purifikáciu zvyčajným spô-
sobom.

Z. Obrad prijimania pod obidvoma spôsobmi
namáěanim

246. Ak je prítomný diakon alebo
iný asistujúci kňaz alebo akolyta:

a) Celebrujúci kňaz mu odovzdá
kalich s purifikatóriom a sám vez-
me paténu alebo cibórium s hos-
tiami. Potom sa kňaz a posluhu-
júci, čo drží kalich, postavia tak,
aby mohli pohodlne dávať veria-
cim prijímanie.

b) Prijímajúci pristupujú po jed-
nom, prejavia náležitú úctu, po-
stavia sa pred kňaza a držia si pa-
ténu pod ústami. Kňaz sčasti na-
močí hostiu v kalichu, pozdvihne
ju a povie Telo a krv Kristova
a prijímajúci odpovie Amen. Od
kňaza prijme Sviatosť a odíde.

c) Prijímanie tých, čo majú pri—
jímať len pod jedným spôsobom,
prijatie zvyšnej Krvi a očistenie
kalicha sa vykoná tak, ako sa už
uviedlo.

247. Ak niet diakona ani iného
asistujúceho kňaza ani akolytu:

a) Celebrant po prijatí Pánovej
krvi vezme cibórium alebo paténu
s hostiami medzi ukazovák a stred-
ný prst lavej ruky a kalich medzi
palec a ukazovák tej istej ruky a
postaví sa tak, aby mohol pohodlne
dávať prijímanie. '

b) Prijímajúci pristupujú po jed-
nom, prejavia náležitú úctu, po-
stavia sa pred celebranta a držia si
paténu pod ústami. Celebrant sčas-
ti namočí hostiu v kalichu, po-
zdvihne ju a povie: Telo a krv

58* VŠEOBECNÉ SMERNICE - NIISÁL

