SPEV NA PRIJÍMANIE (z Kor ;, 15)

Kristus zomrel za všetkých,
aby tí, čo žijú, už nežili pre seba,
ale pre toho, ktorý za nich zomrel
a vstal z mŕtvych. Aleluja.

PO PRIJÍMANÍ

Prosíme ťa, Bože, *

dobrotivo ochraňuj svoj ľud,

ktorý si nasýtil sviatostným pokrmom, —

a pomáhaj mu,

aby prešiel z temnoty hriechu do nového života.
Skrze Krista, nášho Pána.

 

PIATOK
po Piatej veľkonočnej nedeli

 

IWODNY SPEV (Zjv ;, 12)

Hoden je Baránok, ktorý bol zabitý,
prijať moc a bohatstvo, múdrosť a silu,
česť, slávu a chválu. Aleluja.

MODLITBA DNA

Prosíme t'a, dobrotivý Bože, *

pomôž nám dôsledne stvárňovat' náš život

podľa veľkonočného tajomstva,

ktoré s radosťou slávime, —

aby nás sila Kristovho zmŕtvychvstania
ochraňovala a viedla k spáse.

O to ťa prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po Všetky veky vekov.

PIATOK PO PIATE] NEDELI 249

