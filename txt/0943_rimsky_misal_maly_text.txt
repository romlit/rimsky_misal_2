NAD OBETNYMI DARMI

Pane a Bože náš,

slávime pamiatku nesmiernej lásky tvojho Syna *
a pokorne t'a prosíme, —

aby prostredníctvom Cirkvi

ovocie jeho spasiteľného diela

bolo na spásu celému svetu.

Skrze Krista, nášho Pána.

Pieseň vďaky za jednotu kresťanov (str. 870).

SPEV NA PRÍJÍMANIE (Porov. 1 Kor IO, 17)

Aj my všetci, hoci je nás mnoho, sme jedno telo,
lebo všetci máme účasť
na jednom chlebe a na jednom kalichu.

PO PRIJÍMANÍ

Milosrdný Otče,

v tejto obdivuhoánej sviatosti

poskytuješ svojej Cirkvi silu a útechu; *
nech sa tvoj ľud častým svätým prijímaním
vrúcnejšie primkne ku Kristovi, —

aby mohol spĺňať svoje poslanie vo svete

a slobodne budovať tvoje večné kráľovstvo.
Skrze Krista, nášho Pána.

D

ÚVODNY SPEV (Porov. Jn 17, 20-21)

Otče, prosim za tých, čo uveria vo mňa,
aby oni v nás jedno boli,
aby svet uveril, že si ma ty poslal.

ZA SVÁTÚ CIRKEV 843

