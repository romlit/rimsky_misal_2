21. januára

SV. AGNESY, PANNY A MUČENICE

Spomienka

Spoločná omša mučeníkov (str. 713 n.)
alebo panien (str. 746 n.).

MODLITBA DNA

Všemohúci a večný Bože,

ty si volíš, čo svet pokladá za slabé,

aby si zahanbil, čo je v očiach sveta mocné; *
prosíme ťa, dopraj nám vytrvat' vo viere, —

ako nás k tomu povzbudZuje svätá Agnesa,

ktorej mučenícku smrť dnes oslavujeme.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

22. januára
SV. VINCENTA, DIAKONA A MUČENÍKA

Spoločná omša mučeníkov (str. 715 n.).

MODLITBA DNA

Všemohúci a večný Bože, *

láskavo na nás zošli svojho Ducha, -—

aby v našich srdciach zavládla taká láska,

s akou svätý mučeník Vincent

pretrpel všetky telesné muky.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

22. JANUÁRA 487

