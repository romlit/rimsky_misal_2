I) Pri príprave darov sa na oltár
prináša chlieb a víno s vodou, čiže
tie prvky, ktoré Kristus vzal do
svojich rúk.

2) V eucharistickej modlitbe sa
vzdáva Bohu vďaka za celé dielo
spásy a obetované dary sa stávajú
Kristovým telom a krvou.

;) V lámaní Chleba sa prejavuje
jednota veriacich a v prijímaní ve—
riaci prijímajú telo a krv Pána tak
isto, ako ich kedysi prijímali apoš-
toli z rúk samého Krista.

PRÍPRAVA DAROV

49. Na začiatku eucharistickej li-
turgie sa k oltáru prinášajú dary,
ktoré sa stanú Kristovým telom a
krvou.

Predovšetkým sa pripraví oltár
čiže stôl Pána, ktorý je stredom ce-
lej eucharistickej liturgie.“l Naň sa
položí korporál , puriňkatórium, mi-
sál a kalich (ak sa nepripravuje
pri stolíku).

Potom sa prinášajú obetné dary.
Je chválitebné, ked' veriaci sami
prinášajú chlieb a víno. Kňaz alebo
diakon ich prevezme na vhodnom
mieste. Potom sa kladú na oltár
s predpísanými modlitbami. Hoci
chlieb a víno, určené na bohosluž-
bu, veriaci už neprinášajú zo svoj—
ho ako kedysi, jednako obrad pri—
nášam'a týchto darov si zachováva
duchovnú silu a význam.

Možno prijať aj peniaze alebo
iné dary pre chudobných alebo na
kostol, ktoré veriaci prinášajú ale-
bo ktoré sa vyberajú v kostole.
Kladú sa na vhodné miesto mimo
eucharistického stola.

50. Počas obetného sprievodu spie—
va sa priliehavá pieseň. Spieva

sa aspoň dovtedy, kým sa dary ne-
položia na oltár. O tom, ako sa
má spievať, platí to isté ako pri
úvodnom speve (č. 26). Keď sa
pieseň pri príprave obetných darov
nespieva, jednoducho sa vynechá.

51. Na oltári položené dary, ako
aj sám oltár možno okiadzať, aby
sa naznačilo, že obeta Cirkvi a jej
modlitba vystupujú ako vôňa ka—
didla pred Božiu tvár. Po incenzo-
vaní obetných darov a oltára dia-
kon alebo iný prisluhuiúci môže
incenzovat' kňaza aj ľud.

52. Potom si kňaz umýva ruky.
Týmto obradom sa vyjadruje túž-
ba po vnútornom očistení.

53. Po položení obetných darov na
oltár a po vykonaní sprievodných
obradov kňaz vyzve veriacich, aby
sa s ním modlili. Modlitba nad
obetnými darmi uzaviera prípravu
obetných darov a uvádza euchari-
stickú modlitbu.

EUCHARISTICKÁ MODLlTBA

54. Teraz sa začína ústredná a
vrcholná čast' celej bohoslužby —
eucharistická modlitba, modlitba
vzdávania vďaky a posvätenia. Kňaz
vyzýva ľud, aby pozdvihol srdce
k Pánovi modlitbou a vzdávaním
vďaky a pridružuje si ho v modlit-
be, ktorú v mene celého spoločen-
stva a skrze ježiša Krista vysiela
k Bohu Otcovi. Zmyslom tejto
modlitby je, aby sa pri vyznávaní
veľkých Božích skutkov a pri pri-
nášaní obety celé zhromaždenie
veriacich spojilo s Kristom.

55. Hlavné prvky, z ktorých sa
skladá eucharistická modlitba,
možno rozlíšiť takto:

a) Vzdávanie vďaky (vyjadruje

" Porov. Posv. kongr. obradov, inštr. Inter Oetumem'rí, 26. sept. 1964, č. 91: AAS 56 (t964) str. 898;
inštr. Eluharlklícum mystérium, 25. mája 1967, č. 24: AAS 59 (1967) str. 554.

VŠEOBECNÉ SMERNICE - MISÁL 33*

Z'

