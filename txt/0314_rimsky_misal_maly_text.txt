rozmnožuješ počet veriacich V teba; *

láskavo ochraňuj svojich vyvolených,

ktorých si znovuzrodil vo sviatosti krstu, —

a zaodej ich raz rúchom blaženej nesmrteľnosti.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery sa vynechá.

NAD OBETNYMI DARMI

Pane a Bože náš, *

nech nás ustavične sprevádza radosť

z týchto veľkonočných tajomstiev,

ktorými sa neprestajne uskutočňuje naša spása, —
a daj, aby sa nám stali prameňom

večnej blaženosti.

Sere Krista, nášho Pána.

Pieseň vďaky veľkonočné I. (ale slávnostnejšie v tento deň) (str. 367).

Ak sa recituje Rímsky kánon, použije sa vlastná vložka V spoločenstve a
Prosíme ťa, Bože.

SPEV NA PRIJÍMANIE (Gal 3, 27)

Všetci, ktorí ste pokrstení v Kristovi,
Krista ste si obliekli. Aleluja.

PO PRIJÍMANÍ

Prosíme t'a, večný Bože, *

láskavo zhliadni na svoj ľud,

ktorý si duchovne obnovil
veľkonočnými sviatosťami, —

a priveď nás k slávnemu vzkrieseniu.
Skrze Krista, nášho Pána.

214 VEĽKONOČNÉ OBDOBIE

