NAD OBETNYMI DARMI

Bože, dobrotivý Otče,

prinášame ti svätú obetu

za našich zosnulých (M. a M.); *
vypočuj našu pokornú prosbu -

a preukáž im svoje večné milosrdenstvo.
Skrze Krista, nášho Pána.

PO PRIJÍMANÍ

Láskavý Otče,

posilnení nebeskou sviatosťou

pokorne ťa prosíme

za tvojich zosnulých služobníkov (M. a M.); *
pre túto svätú obetu udel' im odpustenie hriechov, —
aby mohli vojst' do tvojho kráľovstva

a večne t'a chváliť v spoločenstve svätých.

Skrze Krista, nášho Pána.

B
MODLITBA DNA

Všemohúci Bože,

odporúčame ti tvojich služobníkov M. a M.,

ktorí už zomreli pre tento svet, *

a prosíme ťa, —

láskavo im odpusť všetky previnenia,

ktorých sa dopustili z ľudskej krehkosti,

aby mohli žiť naveky pre teba.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

994 MODLITBY ZA ZOSNULYCH

