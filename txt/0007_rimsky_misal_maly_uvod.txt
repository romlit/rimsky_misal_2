SLOVENSKÁ LITURGICKÁ KOMISIA

Prot. č. 31/1979

Rímsky misál spolu s lekcionáróm sú hlavnými liturgickými knihami na slávenie eucharistickej obety.

Posvätná kongregácia pre bohoslužbu na základe príkazu pápeža Pavla VI. pripravila v zmysle dekrétov Druhého vatikánskeho koncilu nové vydanie Rímskeho misála, ktorý v latinskej reči zverejnila 26. marca 1970 a v druhom, trocha zmenenom typickom vydaní, dňa 27. marca 1975.

Texty Rímskeho misála preložila Slovenská liturgická komisia do slovenskej reči. Preklad po schválení biskupov a ordinárov Slovenska potvrdila Posv. kongregácia pre sviatosti a bohoslužbu dekrétom
č. CD 1241/79 zo dňa 1. decembra 1979.

Slovenské znenie Rímskeho misála nám láskavo vytlačila Poliglotta Vaticana, kníhtlačiareň Svätej Stolice. Túto vzácnu liturgickú knihu dáva
nám ako dar sám Svätý Otec, pápež Ján Pavol II.

Toto vydanie Rímskeho misála v slovenskej reči sa stáva úradným vydaním pre slovenskú jazykovú oblasť a má sa používať ihned, ako bude k dispozícii.

Banská Bystrica-Nitra,
na slávnosť Zmŕtvychvstania Pána, 15. apríla 1979.

MONS. JOZEF FERANEC
biskup, predseda ZOS

MONS. DR. JÁN PÁSZTOR
biskup, predseda SLK