aj títo novomanželia M. a M. a celá tvoja rodina,
ktorá t'a za nich vrúcne prosí.

Tak, ako si im doprial dožiť sa tejto posvätnej chvíle,
daj im dožiť sa

(radosti z detí, tvojho daru, a)

požehnanej staroby.

(Skrze nášho Pána ]ežiša Krista. Amen.)

MODLITBA POŽEHNANIA
NAD NOVOMANŽELMI

Po modlitbe Pána - Otče náš - sa vynechá Proxíme ťa, Otče, zbav
nás všetkého zla. Kňaz sa obráti k novomanželom a so zloženými rukami
sa modlí tu uvedenú modlitbu požehnania.

V modlitbe Svätý Otče možno podl'a okolností vynechať jeden z dvoch
odsekov (a, b) a modliť sa len ten, ktorý sa zhoduje s čítaním omše.

Prosme Pána za novomanželov,
ktorí pri tomto oltári uzavreli sviatosť manželstva

(a ktorí prijmú Kristovo telo a krv), žeby
sa navzájom vždy milovali úprimnou láskou.

Všetci sa chvíľu ticho modlia. Potom kňaz rozopne ruky a pokračuje:

a) Svätý Otče, ty si stvoril človeka na svoj obraz.
Stvoril si ho ako muža a ženu,

aby obidvaja v jednote tela a srdca

plnili svoje poslanie.

b) Bože, ty nám zjavuješ hĺbku svojej lásky.
Vzájomnou láskou manželov naznačil

si nám zmluvu,

ktorú si uzavrel so svojím ľudom,

a manželstvo tvojich veriacich si povýšil na sviatosť,

ZA ŽENÍCHA A NEVESTU 799

