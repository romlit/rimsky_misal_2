Je naozaj dôstojné a správne, dobré a spásonosné
teba, Pane, velebit' V každom čase,

ale slávnostnejšie v tento čas,

keď sa Kristus obetoval

ako náš veľkonočný Baránok.

Skrze neho sa rodia pre večný život

noví synovia svetla

a veriacim sa otvárajú brány nebeského kráľovstva.
On svojou smrťou zvíťazil nad našou smrťou

a svojím zmŕtvychvstaním

priniesol všetkým nový život.

Preto vykúpené ľudstvo po celom svete

raduje sa z Kristovho vzkriesenia

a v nebi anjeli so zástupmi svätých

neprestajne spievajú na tvoju slávu:

Svätý, svätý, svätý Pán Boh všetkých svetov

VEĽKONOČNÁ HI

O Kristovi, ktorý viacej neumiera
a stále sa prihovára za nás

Táto pieseň vďaky sa používa vo veľkonočnom období.

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

9.<.0.<9.<

Je naozaj dôstojné a správne, dobré a spásonosné
teba, Pane, velebit' v každom čase,

VEĽKONOČNÁ III 369

