NAD OBETNYMI DARMI

Presvätý Bože,

očisti nás od nákazy hriechov

a s úľubou prijmi naše dary; *
nedopusť, aby sme sa dali zlákat'
klamnými radosťami, —

ale veď nás k pravému šťastiu,
ktoré si nám prisľúbil.

Skrze Krista, nášho Pána.

Pieseň vďaky pôstna (str. 360 n.).

SPEV NA PRIJÍMANIE

Ty si dal príkazy, aby sa verne plnili;
kiežby ma moje cesty viedli tak,
žeby som zachovával tvoje ustanovenia.

PO PRIJÍMANÍ

Večný Bože,

s otcovskou láskou nás

nasycuješ nebeským pokrmom; *
posilňuj nás svojou pomocou, —

aby sa ozdravujúca milosť tejto sviatosti

prejavovala aj v našom živote.
Skrze Krista, nášho Pána.

(Ž 118, 4-5)

 

PIATOK

po Tretej pôstnej nedeli

 

ÚVODNý SPEV

Okrem teba nieto Boha, Pane,
lebo si veľký a robíš zázraky;
iba ty si Boh.

108 PôSTNE OBDOBIE

(Ž 85, 8. 10)

