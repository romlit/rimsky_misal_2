NAD OBETNýMI DARMI

Pane a Bože náš, *

nech nás ustavične sprevádza radosť

z týchto veľkonočných tajomstiev,

ktorými sa neprestajne uskutočňuje naša spása, —
a daj, aby sa nám stali prameňom

večnej blaženosti.

Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočné (str. 367 n.).

SPEV NA PRIJÍMANIE (Porov. Lk 24, 46. 26)

Kristus musel trpieť a na tretí deň vstal! z mŕtvych,
a tak vojsť do svojej slávy. Aleluja.

PO PRIJÍMANÍ

Dobrotivý Otče, vypočuj naše prosby, *
aby nás účasť na svätých tajomstvách
nášho vykúpenia

posilňovala v tomto živote -—

a priviedla do večnej radosti.

Skrze Krista, nášho Pána.

 

STREDA
po Šiestej veľkonočnej nedeli

 

ÚVODNY SPEV (Ž 17, 50; 21, za)

Pane, budem ťa velebiť medzi národmi;
tvoje meno chcem zvestovať svojim bratom. Alcluja.

MODLITBA DNA
Bože a Otče náš, *
dožič, aby sme raz so všetkými svätými

STREDA PO ŠIESTE] NEDELI 255

