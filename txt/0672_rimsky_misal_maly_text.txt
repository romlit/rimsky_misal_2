aby sme pod jej ochranou

bezpečne došli k vrcholu dokonalosti,

k Ježišovi Kristovi,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

17. júla

SV. ANDREJA-SVORADA A BENEDIKTA,
PUSTOVNÍKOV

Spomienka
(V Nitrianskej diecéze sviatok hlavných patrónov)

ÚVODNY SPEV (Porov. ž 23, 5-6)

Oslavujme svätých, ktorí dostali požehnanie od Pána
a odmenu od Boha, svojho Spasiteľa, lebo sú z pokolenia, ktoré hľadá Boha.

V Nitrianskej diecéze Oslavná pieseň.

MODLITBA DNA

Láskavý Bože, na tvoje vnuknutie

svätý Andrej-Svorad a Benedikt

sa odobrali do ticha samoty,

aby ti modlitbou, prácou i mlčaním
dokonalejšie slúžili; *

na ich orodovanie pomôž aj nám —
uprostred hluku tohto sveta

započúvat' sa do tvojho hlasu

a dôsledne plniť tvoju vôľu.

Sere nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po Všetky veky vekov.

572 VLASTNÉ OMŠE SVÁTYCH

