priviedol svoj ľud ku kresťanskej viere; —

daj, nech je svojmu ľudu i nám všetkým

mocným orodovníkom V nebeskom kráľovstve.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

19. augusta
SV. JÁNA EUDES, KNAZA

Spoločná omša duchovných pastierov (str. 731 n.)
alebo svätých rehoľníkov (str. 759 n.).

MODLITBA DNA

Večný Bože,

ty si vyvolil svätého kňaza Jána,

aby ohlasoval nevýslovné bohatstvo
Kristovho tajomstva; *

daj, aby sme podľa jeho príkladu a učenia
stále viac poznávali teba —

a žili verne podľa evanjelia.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

20. augusta

SV. BERNARDA, OPÁTA A UČITEĽA CIRKVI

Spomienka

Spoločná omša učiteľov Cirkvi (str. 743 n.)
alebo svätých rehoľníkov (str. 759 n.).

20. AUGUSTA 599

