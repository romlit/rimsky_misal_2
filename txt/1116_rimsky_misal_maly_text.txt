Alebo :

— ]ežišu, ty si cesta, ktorá vedie k Otcovi: Pane,
zmiluj sa.

— Ty si pravda, ktorá osvecuje ľudstvo: Kriste,
zmiluj sa.

— Ty si život, ktorý obnovuje celý svet: Pane,
zmiluj sa.

Alebo :

—- Pane Ježišu, ty máš slová života večného: Pane,
zmiluj sa.

— Ty si tichý a pokorný srdcom: Kriste, zmiluj sa.

— Ty si bol pre nás poslušný až na smrť na kríži:
Pane, zmiluj sa.

Alebo :

— Ježišu, ty si odpustil plačúcemu Petrovi, ktorý
t'a zaprel: Pane, zmiluj sa.

— Ty si prisľúbil nebeské kráľovstvo kajúcemu
zločincovi na kríži: Kriste, zmiluj sa.

— Ty si dal apoštolom Ducha Svätého, aby od-
púšťali hriechy: Pane, zmiluj sa.

Alebo :

— Ježišu, ty si zomrel, aby každý, kto uveri v teba,
mal život večný: Pane, zmiluj sa.

— Ty si prišiel na svet hl'adat', čo sa bolo stratilo:
Kriste, zmiluj sa.

— Ty si bol poslaný od Otca, nie aby si svet súdil,
ale aby si ho spasil: Pane, zmiluj sa.

1016 DODATOK

