A preto Všade ohlasujeme tvoju moc a silu,
ktorou si nás vyviedol z temnôt
do svojho predivného svetla.

Preto s anjelmi, archanjelmi

a so zástupmi všetkých svätých
spievame chválospev na tvoju slávu
a neprestajne voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov...

NEDEĽNÁ 11

O tajomstve spásy

Táto pieseň vďaky sa používa v nedele v obdobi (( cez rok ».

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

.0.<9.<.0.<

je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože,
sere nášho Pána Ježiša Krista.

Lebo on sa zľutoval nad hriešnym ľudstvom,
ponížil sa a narodil sa z Panny.

Trpel na kríži

a oslobodil nás od večnej smrti,

vstal z mŕtvych

a daroval nám večný život.

NEDEĽNÁ II 375

