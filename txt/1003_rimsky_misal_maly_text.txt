Alebo: (Je: 29, u. 12. 14)

Pán hovori:

Mojím úmyslom je priniesť vám blaho, a nie nešťastie.

Keď ma budete vzývat', vyslyším vás

a zhromaždím vás zo všetkých krajov, kde žijete vo vyhnanstve.

MODLITBA DNA

Bože, otec všetkých ľudí,

tebe nikto nie je cudzí ani taký vzdialený,

že by si mu nemohol pomôcť; *

dobrotivo zhliadni na utečencov a vyhnancov,

na ľudí zaznávaných

a na tých, čo sú odlúčení od svojich drahých: —
pomôž im vrátiť sa do vlasti

a nám Vlož do srdca lásku

voči prisťahovalcom a núdznym.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po Všetky veky vekov,

NAD OBETNYMI DARMI

Dobrotivý Otče, *

slávime obetu tvojho Syna, ktorý obetoval život,
aby zhromaždil tvoje roztratené deti

a zmieril ich s tebou; —

nech nás táto obeta duchovne spojí

a rozmnoží V nás bratskú lásku.
Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Ž 90, :)

Ty si moje útočište a pevnosť moja;
v tebe mám dôveru, Bože môj.

ZA UTEČENCOV A VYHNANCOV 903

