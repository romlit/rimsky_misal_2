 

VEĽKY PIATOK

 

SLÁVENIE UTRPENIA A SMRTI PÁNA

r. Podla pradávnej tradície Cirkev v tento a v nasledujúci deň ncslávi
eucharistickú obetu.

z. Oltár má byť úplne obnažený, bez kríža, svietnikov a plachiet.

3. V popoludňajších hodinách okolo tretej (z pastoračných dôvodov
to môže byť aj neskoršie) slávi sa liturgia umučenia Pána. Má tri časti:
liturgiu slova, poklonu svätému krížu a sväté prijímanie.

V tento deň sa veriacim podáva Oltárna sviatosť výlučne v rámci
tejto liturgie; ale chorým, ktorí sa nemôžu na nej zúčastniť, možno sväté
prijímanie zaniesť kedykoľvek.

4. Kňaz a prisluhujúci sa oblečů ako na omšu do rúcha červenej farby;
prídu k oltáru, poklonia sa, l'ahnů si dolu tvárou alebo, ak je to vhod—
nejšie, kl'aknú si, a zotrvajú chvílu v tichej modlitbe.

5. Potom kňaz a prisluhujúci odídu k sedadlám. Kňaz sa obráti k ľudu a
so zopätými rukami sa modlí jednu z týchto modlitieb:

MODLITBA (Vynechá sa výzva: Modlime sa)

Dobrotivý Otče,

rozpomeň sa na veľké skutky svojho milosrdenstva
a ustavične ochraňuj a posväcuj svoj ľud,

za ktorý tvoj Syn Ježiš Kristus vylial svoju krv,
a tak započal veľkonočné tajomstvo.

On žije a kraľuje na veky vekov. O.: Amen.

Alebo:

Milosrdný Bože, *
utrpením a smrťou tvojho Syna

a nášho Pána Ježiša Krista

premohol si smrt', ktorú celé ľudstvo zdedilo
ako trest za prvotný hriech;

prosíme _t'a, urob nás podobnými Kristovi,

a keď sme podľa prirodzenosti

160 VEĽKY TYŽDEN

