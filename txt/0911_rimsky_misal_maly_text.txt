ktorí si toľké roky boli verní

a žili vo svätom spoločenstve; —

aj naďalej ich požehnávaj

a zachovaj v zdraví, svornosti a pokoji.
Skrze Krista, nášho Pána.

Pieseň vďaky spoločná IV. (str. 396).

SPEV NA PRIJÍMANIE (ž „7, :)

Chcem ťa, Pane, oslavovať celým srdcom,
že si vypočul slová mojich úst.

Alebo: (Ž 115, 12-13)

Čím sa odvďačím Pánovi za všetko, čo mi dal?
Vezmem kalich spásy
a budem vzývat' meno Pánovo.

PO PRIJÍMANÍ

Bože, náš Otče,

ďakujeme ti za to,

že si nás posilnil nebeským pokrmom

a duchovným nápojom pri tvojom stole; *
prosíme ťa

za týchto jubilujúcich manželov M. a M.: —
ochraňuj ich v požehnanej starobe

a keď obidvaja naplnia dni života,

priveď ich na večnú hostinu V nebi.

Skrze Krista, nášho Pána.

POŽEHNANIE NA KONCI OMŠE

Nech vám udelí radosť a nech vás (i vaše deti) po-
žehná všemohúci Boh Otec.

O.: Amen.

PRI ZLATOM SOBÁŠI 811

