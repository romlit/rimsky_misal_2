SPOLOČNÉ
OMŠE

1. V jednotlivých skupinách spoločných omší sa uvádzajú z praktických
dôvodov viaceré omšové formuláre so všetkými časťami, totiž so spevmi
a modlitbami.

Ale kňaz, ak to uzná za vhodné, môže spevy a modlitby z tej istej
skupiny spoločných omší medzi sebou zameniť, vyberúc si tie texty,
ktoré sú z pastoračného hladiska primeranejšie.

Okrem toho v omšiach, ktorými sa slávi liturgická spomienka, možno
brať modlitbu nad obetnými darmi a modlitbu po prijímaní nielen zo

spoločných omší, ale aj z feriálnych formulárov príslušného liturgického
obdobia.

2. V spoločných omšiach mučeníkov a v spoločných omšiach svätých
možno všetky modlitby uvedené pre mužov použiť aj pre ženy s pri-
spôsobením rodu.

3. Vo všetkých skupinách spoločných omší možno texty uvedené v jed-
notnom čísle použiť aj pre viacerých v množnom čísle. Podobne aj texty
vyjadrené množným číslom možno použiť aj pre jedného v jednotnom
čísle.

4. Niektoré omše určené pre zvlášť označené obdobia a okolnosti treba
použiť v tých obdobiach a okolnostiach.

5. Vo veľkonočnom období sa na konci úvodného spevu a spevu na pri-
jímanie pridáva jedno Aleluja.

6. V dňoch liturgickej slávnosti alebo sviatku možno ľubovoľne vybrať
niektorý spev z tých, čo sú uvedené na strane 769.

