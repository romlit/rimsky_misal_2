NAD OBETNYMI DARMI

Všemohúci Bože,

posvät' naše obetné dary *

a daj , aby sme účasťou na posvätnej hostine
stále viac poznávali tajomstvá večného Slova, -
ktoré si pri Poslednej večeri

zjavil svojmu apoštolovi Jánovi.

O to ťa prosíme skrze Krista, nášho Pána.

Pieseň vďaky vianočná (str. 356 n.).

SPEV NA PRIJÍMANIE ( ]n ;, WIG)

Slovo sa telom stalo a prebývala medzi nami.
A z jeho plnosti sme prijali všetci.

PO PRIJÍMANÍ

Všemohúci Bože, *

svätý apoštol Ján ohlasoval,

že tvoje Slovo sa stalo telom pre našu spásu; -—
daj, aby skrze posvätné tajomstvo,

ktoré sme slávili,

stále prebýval v nás tvoj Syn Ježiš Kristus,
ktorý s tebou žije a kraľuje na veky vekov.

28. decembra

SVÁTYCH NEVINIATOK, MUČENÍKOV

Sviatok
ÚVODNY SPEV

Nevinné deti,
zabité pre Krista,

28. DECEMBRA 683

