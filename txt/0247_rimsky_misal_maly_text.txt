daj nám účast' aj na jeho slávnom vzkriesení.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARM]

Prosíme t'a, Otče, prijmi naše obetné dary *

a daj, aby nám pamiatka umučenia tvojho Syna,
ktorú nábožne slávime, _

pomáhala odhodlane niesť každodenný kríž.
Skrze Krista, nášho Pána.

Pieseň vďaky o umučení Pána II. (str. 366).

SPEV NA PRIJÍMANIE (Mt zo, 28)

Syn človeka neprišiel dať sa obsluhovať,
ale slúžiť a položiť svoj život ako výkupné za mnohých.

PO PRIJÍMANÍ

Všemohúci Bože, touto posvätnou obetou sme slávili
tajomstvo nášho vykúpenia; *

prosíme t'a, _

posilňuj nás vo viete,

že pre zásluhy smrti tvojho Syna

dal si nám večný život.

Skrze Krista, nášho Pána.

STREDA 147

