aby som si s čistou mysľou a telom

zaslúžil prijať Najsvätejšiu sviatosť.

A daj, aby ma toto sväté požívanie Tela a Krvi,
ktoré, ja nehodný, túžim prijať,

oslobodilo od hriechov,

dokonale očistilo od previneni,

aby zahnalo nečisté predstavy,

vzbudilo dobré myšlienky,

bolo spasiteľným prameňom tebe milých skutkov
a najistejšou ochranou duše i tela

pred osídlami mojich nepriatelov.

Amen.

MODLITBA SVÁTÉHO TOMÁŠA AKVINSKÉHO

Všemohúci a večný Bože,

pristupujem k sviatosti tvojho jednorodeného Syna
a nášho Pána Ježiša Krista

ako nečistý k prameňu milosrdenstva,
nevidomý k svetlu večnej jasnosti,

chudobný a biedny k Pánovi neba i zeme.
Preto prosím tvoju bohatú a nesmiernu štedrosť,
láskavo vylieč moje neduhy,

obmy moju nečistotu,

osviet' moju nevidomost',

obohat' moju chudobu,

zaodej moju nahotu,

aby som mohol prijať anjelský chlieb,

Kráľa kráľov, Pána nad vládcami,

s takou úctivost'ou a pokorou,

s takou skrúšenosťou a nábožnosťou,

s takou čistotou a vierou

a s takým predsavzatím a úmyslom,

žeby to bolo na spásu mojej duše.

Pomôž mi, prosím, prijat nielen sviatosť Pánovho tela a krvi,
ale aj jej milosť a silu.

Láskavý Bože,

daj, aby som tak požíva] telo tvojho jednorodeného Syna,
nášho Pana ]ežiša Krista,

ktoré prijal z Márie Panny,

žeby som si zaslúžil začleniť sa do jeho tajomného tela

a patriť k jeho údom.

PRÍPRAVA NA OMŠU 1027

