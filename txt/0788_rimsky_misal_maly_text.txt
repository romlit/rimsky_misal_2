SPOLOČNÉ OMŠE POSVÁTENIA CHRÁMU

VŠLDEN POSVÁTENIA

Omšové formuláre pre slávnosť posvätenia chrámu a posvätenia oltára
sa nachádzajú v omšiach pri vysluhovaní sviatostí a svätenín (str. 829-836).

A
VYROČIE POSVÁTENIA CHRÁMU

V chráme, ktorého Výročie posvätenia sa slávi

ÚVODNY SPEV (ž 67, 36)

Vznešený je Boh vo svojej svätyni, Boh Izraela,
on sám dáva silu i statočnosť svojmu ľudu.
Nech je zvelebený Boh.

Oslavná pieseň.

MODLITBA DNA

Všemohúci Bože, pre tvoju dobrotu

môžeme každoročne sláviť

deň posvätenia tohto chrámu; *

vyslyš prosby svojho ľudu a dopraj, —

aby sa na tomto mieste

vždy konala tebe milá služba

a na nás hojne zostupovala milosť vykúpenia.
Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou. žije & kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

688 SPOLOČNÉ OMŠE

