SPEV NA PRIJÍMANIE (Lk 1, 48-49)

Blahoslaviť ma budú všetky pokolenia,
lebo veľké veci mi urobil ten, ktorý je mocný
a sväté je jeho meno.

PO PRIJÍMANÍ

Dobrotivý Otče,

zvelebujeme t'a s Pannou Máriou a s celou Cirkvou,
lebo si nás zahrnul veľkými milosťami; *

ako svätý Ján zaplesal, keď pocítil prítomnosť
Božieho Syna V lone panenskej matky, —

daj, nech aj my s radosťou prijímame Ježiša Krista,
vždy prítomného v Oltárnej sviatosti.

O to t'a prosíme skrze Krista, nášho Pána.

3. júla
SV. TOMÁŠA, APOŠTOLA
Sviatok
ÚVODNY SPEV (ž 117, 28. 21)

Ty si môj Boh, vďaky ti vzdávam;
ty si môj Boh, velebím ťa.
Ďakujem ti, že si ma zachránil.

Oslavná pieseň.

MODLITBA DNA

Všemohúci Bože,

s radosťou slávime sviatok svätého apoštola Tomáša,
ktorý vyznal, že tvoj Syn je Pán a Boh; *
prosíme ťa, nech nás ustavične sprevádza

ochrana tohto apoštola, —

aby sme vierou mali život

562 VLASTNÉ OMŠE SVÁTY'CH

