prosíme ťa, daj, aby nás ich hrdinský zápas
povzbudzoval k statočnosti —

a ich sväté víťazstvo napĺňalo radosťou.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

560 VLASTNÉ OMŠE SVÁTY'CH

