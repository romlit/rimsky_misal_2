Pieseň vďaky pôstna (str. 360 n.).

SPEV NA PRIJÍMANIE (]n 3, 17)

Boh neposlal Syna na svet, aby svet odsúdil,
ale aby sa skrze neho svet zachránil.

PO PRIJÍMANÍ

Milostivý Bože,

dávaš nám nebeský pokrm

ako posilu na životnej púti; *

prosíme ťa, —

nedopusť, aby nám bola na odsúdenie sviatosť,
ktorú si nám určil na spasenie.

O to ťa prosíme skrze Krista, nášho Pána.

ŠTVRTOK
po Štvrtej pôstnej nedeli

ÚVODNY SPEV (2 104, 5-4)

Nech sa radujú srdcia tých, čo hľadajú Pána.
Hľadajte Pána a jeho moc, hľadajte vždy jeho tvár.

MODLITBA DNA

Dobrotivý Bože,

V tejto pôstnej dobe nám ponúkaš milosť
napraviť sa pokáním

a posvätiť sa dobrými skutkami; *

pokorne ťa prosíme, —

pomáhaj nám verne zachovávať tvoje prikázania,
aby sme mohli s úprimným srdcom sláviť
veľkonočné sviatky.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

ŠTVRTOK PO ŠTVRTE] NEDELI 117

