splnenie všetkých práv, prosme
Pána.

* Za ťažkopracujúcich a trpiacich
—- aby sa im dostalo všetkej mož-
nej úľavy, prosme Pána.

d) * Za nás všetkých — aby sme
sa tešili z viery v zmŕtvychvstalého
Krista a v tejto blaženej viere oča-
kávali večnú blaženosť s Kristom,
prosme Pána.

* Za nás všetkých — aby sme verne
nasledovali nášho dobrého Pastie-
ra ježiša Krista, a tak spolupra-
covali na zjednotení kresťanov v je-
ho jedinej Cirkvi, prosme Pána.

* Za nás všetkých -— aby sme rástli
v Božom priateľstve zachovávaním
všetkých prikázaní, najmä veľkého
prikázania lásky, prosme Pána.

ZÁVEREČNÁ MODLITBA

a) Bože, vyslyš modlitby svojej
Cirkvi a daj, aby sme stále rástli
vo veľkonočnej milosti. Skrze Kris-
ta, nášho Pána.

b) Milosrdný Bože, tvoj Syn nám
zjavil svoju božskú moc, ked" vstal
z mŕtvych; daruj na'm milosť, aby
sme svojím kresťanským životom
boli svedkami jeho vzkriesenia.
Skrze Krista, nášho Pána.

c) Dobrotivý Bože, kedže ťa môže-
me v Duchu Svätom nazývať svo-
jím Otcom, vypočuj naše prosby,
ktoré ti spoločne prednášame, a
rozhojni v nás vel'konočnú radosť.
Skrze Krista, nášho Pána.

NEDELE V OBDOBÍ »CEZ ROK<<

ÚVODNÁ MODLITBA

a) Bratia a sestry, prosme s dôve—
rou nebeského Otca v mene ježiša
Krista, jeho Syna, o milosti po-
trebné k pokojne'mu spolunažíva-
niu vo svete a v Cirkvi.

b) Bratia a sestry, modlime sa
k Bohu, ktorý v nás prebýva ako
v chráme, aby sme vždy lepšie po-
znávali duchovné bohatstvá, ktorý-
mi nás zahŕňa.

c) Bratia a sestry, v tejto spoločnej
modlitbe prosme Pána, ktorý roz-
sieva semeno svojho slova, aby
sme mali srdce vždy otvorené jeho
milostiam.

d) Drahí bratia a sestry, prosme
nebeského Otca skrze jeho Syna,
ktorý je korunou všetkých svätých,
aby nám dal milosť svätého života.

ÚMYSLY

a) * Za svätú Cirkev — aby posvä-
tená Kristovou láskou tiahla všet-
kých ľudí k Bohu, prosme Pána.
* Za pápeža, biskupov a kňazov
— aby im Kristus pomáhal pozná-
vať, chápať a posväcovat' všetok
im zverený ľud, prosme Pána.

* Za pápeža, biskupov a kňazov
— aby Božím slovom a sviatosťami
sýtili veriacich a horlivo sa starali
o návrat poblúdených, prosme
Pána.

* Za Svätého Otca M. — aby posil-
nený dôverou v Krista Spasiteľa
viedol s istotou lod'ku Cirkvi v na-
šich rozbúrených časoch, prosme
Pána.

MODLITBA VERIACICH 1023

