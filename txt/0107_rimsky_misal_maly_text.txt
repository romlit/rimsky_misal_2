a čo ti teraz so synovskou oddanosťou obetujeme, —
nech nám prinesie večnú spásu.

Skrze Krista, nášho Pána.

Pieseň vďaky adventná 1. (str. 554).

SPEV NA PRIJÍMANIE (Porov. Ž IOS, 4-5; Iz 38, 3)

Príď, Pane, a navštív nás so svojím pokojom;
tvoja prítomnosť nás naplní veľkou radosťou.

PO PRIJÍMANÍ

Pane a Bože náš, nech je nám na osoh

účasť na tejto sviatostnej hostine, *

pri ktorej nás už v tomto pominuteľnom svete
učíš milovat' veci nebeské —

a hľadať hodnoty trváce.

Skrze Krista, nášho Pána.

UTOROK
po Prvej adventnej nedeli

ÚVODNY SPEV (Porov. Zach 14, 5. 7)

Hl'a, Pán príde a s ním všetci jeho svätí;
v ten deň zažiari veľké svetlo.

MODLITBA DNA

Všemohúci Bože,

dobrotivo vyslyš naše pokorné prosby, *

pomôž nám v našich slabostiach —

a daj, aby nás radosť z príchodu tvojho Syna
chránila pred nebezpečenstvami hriechu.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

UTOROK PO PRVEJ NEDELI 7

