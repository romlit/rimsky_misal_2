ktorý sme prijali z tvojho oltára, -—

a nech nás upevní vo vernosti k evanjeliu,
ktoré ohlasoval svätý Lukáš.

O to t'a prosíme skrze Krista, nášho Pána.

x9 . októbra

sv. JÁNA DE BRÉBEUF A IZÁKA ]OGUES,
KNAzov, A ICH SPOLOČNÍKOV, MUČENÍKOV

Spoločná omša mučeníkov (str. 706 n.)
alebo duchovných pastierov-misionárov (str. 738 n.).

MODLITBA DNA

Dobrotivý Bože, ty si posvätil

prvotiny viery v krajinách severnej Ameriky
kázaním a mučeníckou krvou

svätého Jána, Izáka a ich spoločníkov; *
milostivo dopraj, —

aby na ich orodovaníe

vzrastalo a prekvitalo kresťanstvo

vo všetkých končinách Zeme.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

SV. PAVLA Z KRÍŽA, KNAZA

ÚVODNY SPEV (! Kor z, z)

Rozhodol som sa,

že nechcem medzi vami vedieť nič iné
okrem Ježiša Krista,

a to ukrižovaného.

644 VLASTNÉ OMŠE SVÁTýCH

