O to ťa prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

sv. ]ÁNA FIŠERA, BISKUPA
A TOMÁŠA MORUSA, MUČENÍKOV

Spoločná omša mučeníkov (str. 706 n.).

MODLITBA DNA

Láskavý Bože,

mučenícka smrt" je pred tebou

najdokonalejším svedectvom pravej viery; *
pomáhaj nám na orodovanie svätého ]ána Fišera
a Tomáša Morusa, —

aby sme v životných

skúškach skutkami dokazovali vieru,

ktorú vyznávame slovami.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

24. júna

NARODENIE SV. ]ÁNA KRSTITEĽA

Hlavného patróna Trnavskej arcidiecézy

Slávnosť

Omša na vigíliu

Tento omšový formulár sa používa 23. júna, a to pred prvými
vešperami tejto slávnosti alebo po nich.

24. JÚNA 549

