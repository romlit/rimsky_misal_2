koncil pripomenul, že treba za-
chovávať niektoré nariadenia Tri-
dentského koncilu, ktoré sa niekde
nedodržiavali, ako napr. povinnosť
konať homíliu v nedeľu a vo svia-
tok 17 a možnosť vsunút' nejaké po-
naučenie do samých posvätných
obradov.18

Predovšetkým však Druhý vati—
kánsky koncil odporúčal << tú do-
konalejšiu účasť na omši, ktorou
po kňazovom prijímaní aj veriaci
prijímajú Kristovo telo z tej istej
obety »,19 čím pomohol uskutočniť
aj iné želanie tridentských Otcov,
a to, aby pre plnšiu účasť na svätej
Eucharistii << veriaci, prítomní na
svätej omši, prijímali Eucharistiu
nielen duchovne, ale aj sviatost—
ne ».?"

14. Z tých istých pohnútok a 2 to-
ho istého pastoračného úsilia Dru-
hý vatikánsky koncil mohol 1 no-
vého hladiska zvážiť ustanove-
nia Tridentského koncilu 0 pri—
jímaní pod obidvoma spôsobmi.
Pretože sa neuvádza do pochyb—
nosti náuka o účinnosti prijímania
Eucharistie len pod spôsobom
chleba, dovolil v niektorých prípa-
doch prijímat' pod obidvoma spô—
sobmi, a to preto, že jasnejšia for-
ma sviatostného znaku poskytuje
osobitnú možnost' hlbšie vniknúť
do tajomstva, na ktorom sa veriaci
zúčastňujú.21

15. Cirkev tak ostáva verná svojmu
poslaniu, pretože ako učiteľka
pravdy zachováva << staré », t. j. po—
klad tradicie, a zároveň plní svoju
povinnosť tým, že zvažuje a múdro

používa veci << nové » (porov. Mt
13, 52)-

Jedna časť nového misála vhod-
nejšie prispôsobuje modlitby Cir-
kvi potrebám našich čias; také sú
predovšetkým omše pri vysluho-
vaní sviatostí a svätenín a omše
na rozličné príležitosti, v ktorých sa
tradícia vhodne spája s novými
prvkami. Zatiaľ čo viaceré litur-
gické texty, vybrané z najstaršej
cirkevnej tradície a uvedené v mno-
hých vydaniach Rímskeho mi—
sála, zostali nezmenené, iné po—
četné texty boli prispôsobené dneš—
ným požiadavkám a podmien—
kam, a ďalšie texty, ako modlitby
za Cirkev, za laikov, za posvätenie
ludskej práce, za spoločenstvo všet-
kých národov, za niektoré potreby
príznačné našej dobe, boli zasa
zostavené úplne nanovo, pričom sa
použili myšlienky a často aj výrazy
z najnovších koncilových doku-
mentov.

Pri použití textov najstaršej
tradície zaiste sa neurobila krivda
tomuto úctyhodnému pokladu tým,
že sa z ohladu na terajšiu situáciu
vo svete niektoré vety pozmenili,
aby aj spôsob vyjadrovania bol
v súlade s rečou dnešnej teológie
a verne odzrkadľoval súčasný stav
cirkevnej disciplíny. Preto boli po-
zmenené niektoré výrazy týkajúce
sa hodnotenia a užívania pozem-
ského dobra, ako aj výrazy, ktoré
vyjadrujú nejaký vonkajší spôsob
pokánia, používaný v iných obdo-
biach Cirkvi.

Predpisy Druhého vatikánskeho
koncilu v mnohých bodoch teda

" Druhý vatik. koncil, konšt. o posv. liturgii Sat,-romanum Cancílium, č. 52.

“ Tamže, č. 35, 3.
" Tamže, č. 55.

" Trid. koncil, sesia XXII, Učenie o obete sv. omše, kán. 6.
" Porov. Druhý vatik. koncil, konšt. o posv. liturgii Sacrommlum Conríh'um'. č. 55.

VŠEOBECNÉ SMERNICE - MISÁL 23*

