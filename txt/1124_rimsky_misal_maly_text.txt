* Za všetkých cirkevných pred-
stavených — aby nás utvrdzovali
v jednote s rímskym pápežom a
v bratskom spolunažívaní, prosme
Pána.

b) * Za celý svet - aby bol uchrá-

nený od hladu a vojny, prosme
Pána.

* Za celé ľudstvo _ aby žilo vo
svornosti, povzbudene' prikladnou
láskou Kristových vyznavačov,

prosme Pána.

* Za svetských predstavených —
aby nikto z nich nekládol prekážky
hlásateľom Kristovej náuky, pros-
me Pána.

* Za svetské vrchnosti —aby účin-
ne viedli ľud k spoločnému blahu
a neodvádzali ho od večnej vlasti,
prosme Pána.

c) * Za tých, čo opustili cestu spá-
sy — aby využili čas, ktorý im
milosrdný Boh dáva, ku kajúcnosti
a k návratu, prosme Pána.

* Za všetkých pracujúcich — aby
Božie požehnanie sprevádzalo prá-
cu ľudských rúk a ľudského umu,
prosme Pána.

* Za rehoľné osoby - aby v mod-
litbe hľadali silu pre apoštolát
dobrého príkladu v Cirkvi, prosme
Pána.

* Za kňazské povolania — aby
Duch Svätý oduševnil povolaných
mladíkov a pripravil horlivých
robotníkov do duchovnej žatvy,
prosme Pána.

* Za deti a mládež — aby v častom
prijímaní Eucharistie našli prameň
sily a úprimnej lásky k Bohu,
prosme Pána,

1024 DODATOK

* Za chorých a zomierajúcich —
aby s vierou prijali Najsvätejšiu
sviatosť ako záloh slávneho vzkrie-
senia, prosme Pána.

d) * Za nás všetkých — aby sme
sa obohacovali skutkami dobročin-
nej lásky a neskladali svoju dô-
veru V*bohatstvo, prosme Pána.

* Za nás všetkých — aby sme
vždy a všade odvážne vyznávali
svoju vieru a nikdy ju nezapreli,
prosme Pána.

* Za nás všetkých — aby sme
ustavične rástli vo svätosti vytrva—
lou a poníženou službou Bohu a
blížnemu, prosme Paua.

* Za nás všetkých — aby sme sa
vedeli obetovať za našich blížnych
ako Kristus, ktorý obetoval seba
samého za život sveta, prosme
Pána.

ZÁVEREČNÁ MODLITBA

a) Prosíme t'a, Pane, milostivo vy-
počuj prosby svojho ľudu a vo
svojej štedrosti daj nám všetky
dary, 0 ktoré t'a z tvojho vnuk-
nutia prosíme. Skrze Krista, nášho
Pana.

b) Nebeský Otče, ty si zjavil svoje
tajomstvá skromným a pokorným
ľuďom; prosíme t'a, urob aj naše
srdcia tichými a pokornými, zapáľ
v nás oheň svojej lásky a ved nás
k výšinám kresťanskej dokonalosti.
Skrze Krista, nášho Pana.

c) Bože, ty nam dávaš istotu, že
Cirkev, postavenú na Petrovej ska-
le, nepremôžu ani pekelné moc-
nosti; posílňuj nás, aby sme ne-
podľahli pokušeniam, ale vytrvali

