uskutočňuje sa v nás dielo vykúpenia.
Skrze Krista, nášho Pána.

n. PIESEN VĎAKY () Eucharistii I. (str. 38x).

Ak sa recituje Rímsky kánon, použijú sa vlastné vložky V spoločenstve,
Prosíme ťa, Bože a On večer pred svojím umučením.

 

z.k V spoločenstve s celou Cirkvou
v presvätý deň,
keď sa náš Pán Ježiš Kristus
vydal za nás na smrť,
s úctou si spomíname
najmä na preblahoslavenú Máriu, vždy Pannu,
Rodičku Ježiša Krista,
Boha a nášho Pána,
i na svätého Jozefa, jej ženícha,
a na tvojich svätých apoštolov a mučeníkov
Petra a Pavla, Ondreja,
(Jakuba, Jána, Tomáša,
Jakuba, Filipa, Bartolomeja,
Matúša, Šimona a Tadeáša,
Lína, Kléta, Klimenta, Sixta,
Kornélia, Cypriána, Vavrinca, Chryzogóna,
Jána a Pavla, Kozmu a Damiána)
i na všetkých tvojich svätých.
Pre ich zásluhy a na ich prosby
poskytni nám Vždy a všade
svoju pomoc a ochranu.
(Skrze nášho Pána Ježiša Krista. Amen.)

Pokračuje s rozopätými rukami:

H1 Bože, milostivo prijmi túto obetu,
ktorú ti predkladáme my, tvoji služobníci,

156 VEĽKý TYŽDEN

