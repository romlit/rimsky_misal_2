ÚvonNý SPEV (Ž 43. 24-27)

Vstaň, Pane, prečo spíš?

Vstaň a nezavrhni nás navždy.

Prečo odvraciaš svoju tvár?

Prečo zabúdaš na našu biedu a naše súženie?
Ved naša duša je pokorená až do prachu,

a naše telo väzí v zemi.

Vstaň, Pane, pomôž nám

a zachráň nás, veď si milosrdný.

MODLITBA DNA

Všemohúci a milosrdný Bože, *

láskavo zhliadni na naše trápenia,

uľahčí naše bremená

a posilní našu vieru, ——

aby sme sa s neochvejnou dôverou spoliehali

na tvoju otcovskú starostlivosť.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Dobrotivý Bože, *

prijmi obetné dary, ktoré ti s dôverou prinášame, —
a vlož aj naše trpkosti a žiale

do vznešenej obety tvojho Syna.

O to t'a prosíme skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Jn 16, 23. 24)

Ak budete o niečo prosiť Otca v mojom mene,
dá vám to.
Proste a dostanete, aby vaša radosť bola úplná.

V KAŽDEJ NÚDZI 913

