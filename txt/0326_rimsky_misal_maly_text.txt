čo zodpovedá ich kresťanskému povolaniu.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Nebeský Otče, prijmi naše modlitby,

ktorými sprevádzame tieto obetné dary, *

a láskavo očistí naše srdcia, —

aby sme mohli dôstojne sláviť sviatosť tvojej lásky.
Skrze Krista, nášho Pána.

Pieseň vd'aky veľkonočné. (str. 367 n.).

SPEV NA PRIJÍMANIE (I;: 14, 27)

Pokoj vám zanechávam, svoj pokoj vám dávam;
nie taký pokoj vám dávam, aký dáva svet, hovorí Pán. Aleluja.

PO PRIJÍMANÍ

Všemohúci a večný Bože,

zmŕtvychvstaním tvojho Syna

znovu si nám otvoril cestu do večného života; *
Zvel'ad'uj v nás účinky veľkonočného tajomstva —
a naplň nás silou sviatostného pokrmu,

ktorý sme prijali.

Skrze Krista, nášho Pána.

 

UTOROK
po Tretej veľkonočnej nedeli

 

ÚVODNY SPEV (Zjv 19, 5; Iz. m)

Vzdajte chválu nášmu Bohu všetci, čo sa ho bojíte, malí i veľkí,
lebo prišla spása, moc a vláda jeho Pomazaného. Aleluja.

226 VEĽKONOČNÉ OBDOBIE

