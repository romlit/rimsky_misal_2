Pieseň vďaky o Eucharistii (str. 381; 382).
SPEV NA PRIJÍMANIE (Jn 6, 57)

Pán Ježiš povedal:
Kto je moje telo a pije moju krv,
ostáva vo mne a ja v ňom.

PO PRIJÍMANÍ

Dopraj nám, Pane Ježišu, *

aby sme vo večnosti dosiahli plnú radosť
z tvojho božského života, -—

ktorú nám už teraz dávaš okúsiť

v prijímaní tvojho predrahého tela a krvi.
Ty žiješ a kraľuješ na veky vekov.

Kde slávnosť Najsv. Kristovho tela a krvi nie je prikázaným sviatkom,
tam sa jej vyhradzuje ako vlastný deň nedeľa po slávnosti Najsv. Trojice.

332 NAJSV. KRISTOVHO TELA A KRV'I

