NAD OBETNYMI DARMI

Všemohúci Bože,

láskavo prijmi dary svojej Cirkvi, *

ktorá vo vtelení tvojho jednorodeného Syna
vidí svoj počiatok; —

daj, aby sme s radosťou slávili

toto hlboké tajomstvo Ježiša Krista,

ktorý žije a kraľuje na veky vekov.

PIESEN VĎAKY

O taj omstve Vtelenia

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich 11 Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
.: Je to dôstojné a správne.

OŠPŠPŠ

Je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože,
sere nášho Pána Ježiša Krista.

Lebo anjel z neba Zvestoval Panne Márii,

že tvoj Syn sa pôsobením Ducha Svätého

stane človekom,

aby spasil všetkých ľudí.

Ona s vierou prijala nebeské posolstvo

a s láskou nosila v nepoškvrnenom lone Ježiša Krista,
ktorý splnil prisľúbenia dané vyvolenému ľudu

a zjavil sa svetu ako očakávaný Vykupitel' .

512 VLASTNÉ OMŠE SVÁTYCH

