oznamy, ak ich radšej nechce vy—
hlásiť sám kňaz.
140, Po kňazovom požehnaní dia-

kon prepustí ľud slovami Iďte v me-
ne Božom.

141. Potom spolu s kňazom po-
bozká oltár, urobí náležitú poklonu
a vráti sa do sakristie v tom istom
poradí, ako prišiel k oltáru.

C) Služba akolytu

142, Úlohy, ktoré môže vykonávať
akolyta, sú rozličného druhu a mô-
že sa stať, že mnohé z nich treba
vykonať v tom istom čase. Preto je
vhodné rozdeliť ich medzi viace-
rých. Ak je prítomný iba jeden
akolyta, nech sám vykonáva vý-
znamnejšie úlohy a ostatné nech sa
rozdelia medzi iných posluhu-
júcich.

Úvodné obrady

143_Ked' sa ide k oltáru, môže
niesť kríž uprostred dvoch poslu—
hujúcich, ktorí nesú zapálené svie—
ce. Keď príde k oltáru, kríž po-
staví blízo oltára a zaujme svoje
miesto.

144. Počas bohoslužby akolyta má
byť naporúdzi kňazovi alebo dia-
konovi, aby mu podal knihu a
pomáhal mu pri ostatných úko-
noch. Preto nech sa postaví tak -—-
nakoľko je to možné — aby mohol
čo najľahšie konať svoju službu či
už pri sedadle alebo pri oltári.

Liturgia Eucharistie

[45_ Po skončení modlitby veria—
cich akolyta — ak nie je prítomný
diakon — položí na oltár korporál,

puriíikatórium, kalich a misál.
Kňaz je zatiaľ pri sedadle. Potom,
ak treba, pomáha kňazovi pri pri-
jímaní darov ľudu a — podľa okol-
ností — prinesie k oltáru chlieb a
víno a podá kňazovi. Ak sa použí-
va kadidlo, podá ho kňazovi a po-
máha mu pri incenzácii obetných
darov a oltára.

146_ Môže pomáhať kňazovi aj pri
rozdávaní prijímania ľudu ako mi-
moriadny vysluhovatel'.61 Ak sa
prijímanie dáva pod obidvoma
spôsobmi, on podáva prijímajúcim
kalich alebo, ak sa prijímanie dáva
namáčaním hostie, drží kalich.

147, Po prijímaní pomáha kňazovi
alebo diakonovi pri čistení a uspo-
riadaní posvätných nádob. Ak nie
je prítomný diakon, akolyra odne-
sie posvätné nádoby na stolík a
tam ich očistí a usporiada.

D) Služba lektora

148, Keď sa ide k oltáru, môže
niesť — ak nie je prítomný dia-
kon — knihu evanjelií. Ak nesie
knihu, kráča pred kňazom; ináč
ide spolu s ostatnými posluhu-
júcimi.

149, Keď príde k oltáru, po prísluš-
nej úcte, ktorú koná spolu s kňa-
zom, vystúpi k oltáru a položí naň
knihu evanjelií. Potom zaujme
svoje miesto v presbytériu spolu
s ostatnými posluhujúcimi.

Liturgia slova

150. Čítania pred evanjeliom číta
na ambone. Ak nie je prítomný
žalmista, môže po prvom čítaní
predniesť responzóriový žalm.

'" Porov. Pavol VI., apošt. list Miniuvría quaedam, 15. augusta 1972, č. VI: AAS 64 (X971) str. 531.

46* VŠEOBECNÉ SMERNICE _ MISÁL

