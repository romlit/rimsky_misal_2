pomáhaj nám svojou milosťou tak žiť, —

aby sme boli tvojím dôstojným príbytkom.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

NAD OBETNYMI DARMI

Prosíme t'a, milosrdný Bože, *

nech nás táto obeta očistí a obnoví —

a nech nám zabezpečí večnú odmenu,
ktorú si prisľúbil tým, čo plnia tvoju vôľu.
Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná (str. 374 n.).

SPEV NA PRIJÍMANIE (Porov. Ž 77, 29-30)

Pán Boh splnil túžbu svojho ľudu:
prijali pokrm a nasýrili sa,
ich očakávanie sa splnilo.

Alebo: (]n 3, 16) _

Boh tak miloval svet,

že dal svojho jednorodeného Syna,
aby nezahynul nik, čo v neho verí,
ale aby mal večný život.

PO PRIJÍMANÍ

Bože, s radosťou sme sa zúčastnili

na eucharistickej hostine *

a prosíme ťa, —

daj, aby sme vždy túžili po tomto pokrme,
ktorý v nás zveľaďuje život milosti.

Skrze Krista, nášho Pána.

ŠIESTA NEDEĽA 287

