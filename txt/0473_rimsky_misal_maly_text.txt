dostaneme do nebeskej vlasti,

kam nás on predišiel

ako naša Hlava a pôvodca našej spásy.
Preto vykúpené ľudstvo po celom svete
raduje sa z Kristovho vzkriesenia

a v nebi anjeli so zástupmi svätých
neprestajne spievajú na tvoju slávu:

Svätý, svätý, svätý Pán Boh všetkých svetov...

V deň Nanebovstúpenia sa používa v Rímskom kánone vlastná vložka
V spoločenstve (str. 410).

o NANEBOVSTÚPENÍ PÁNA 11

O tajomstve Nanebovstúpenia

Táto pieseň vďaky sa používa v deň Nanebovstúpenia a v omšiach po
dni Nanebovstúpenia až do soboty pred Zaslaním Ducha Svätého, ktoré
nemajú vlastnú pieseň vďaky.

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Márne ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

PŠPŠPŠ

Je naozaj dôstojné a správne, dobré a spásonosné,
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože,
skrze nášho Pána Ježiša Krista.

Lebo on sa po svojom zmŕtvychvstaní
viditeľne zjavil všetkým svojim učeníkom
a pred ich očami vzniesol sa do neba,

O NANEBOVSTÚPENÍ PÁNA II 373

