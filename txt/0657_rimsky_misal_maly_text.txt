SPEV NA PRIJÍMANIE (Jn 21, 15. 17)

Šimon, syn Jánov, miluješ ma väčšmi ako tito?
Pane, ty všetko vieš, ty vieš, že ťa milujem.

PO PRIJÍMANÍ

Láskavý Bože, vrúcne t'a prosíme, *

ustavične nás posilňuj nebeským pokrmom, —
aby sme sa držali náuky apoštolov.

Skrze Krista, nášho Pána.

Omša v deň slávnosti

ÚVODNý SPEV

Oslavujeme apoštolov, ktorí položili základy Cirkvi
a zvlažilí ich vlastnou krvou;

pili z kalicha Pánovho utrpenia

a stali sa Božími priateľmi.

Oslavná pieseň.

MODLITBA DNA

Všemohúci a večný Bože,

dnešnou slávnosťou svätého Petra a Pavla

pripravil si nám veľkú a svätú radosť; *

pomáhaj svojej Cirkvi, —

aby vo všetkom nasledovala

učenie svätých apoštolov,

od ktorých pri svojom zrode prijala dar viery.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

29. JÚNA 557

