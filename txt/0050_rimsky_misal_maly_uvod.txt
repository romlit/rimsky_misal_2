pozdvihovaní pozrú na hostiu a na
kalich a potom sa hlboko uklonia;

d) Keď teda slávime a Pokorne
t'a prosíme hovoria s rozopnu—
tými rukami.

181. Príhovory za živých Pamätaj,
Otče, a za mŕtvych Pamätaj ina
našich bratov a sestry možno zve-
rit' jednému alebo dvom konceleb-
rantom a každý osve prednesie tieto
modlitby s rozopnutými rukami.

182. Texty tejto eucharistickej
modlitby: On, prv než sa dobro-
voľne vydal na smrť, Podobne po
večeri a Keď teda slávime, ako aj
zakončujúcu doxológiu možno
spievať.

C) Tretia eucharistická modlitba

183. Naozaj si svätý hovorí len
hlavný celebrant s rozopnutými
rukami.

184. Od Preto t'a, Otče, pokorne
prosíme až po Zhliadni, prosíme
koncelebranti hovoria všetko spolu
takto:

a) Preto t'a, Otče, pokorne pro-
síme s rukami vystretými smerom
k darom.

b) On v tú noc, keď bol zradený
a Podobne po večeri so zopnu-
tými rukami.

c) Pánove slová vyslovujú s pra-
vicou vystretou smerom k Chlebu
a ku kalichu, ak je to vhodné; pri
pozdvihovaní pozrú na hostiu a na
kalich a potom sa hlboko uklonia.

d) Preto, Otče, keď slávime a

Zhliadni, prosíme hovoria s rozop-
nutými rukami.

185. Príhovory Nech Duch Svätý
a Prosíme ťa, Otčc, nech táto
obeta možno zveriť 'jedne'mu
alebo dvom koncelebrantom a kaž-

dý osvc prednesie tieto modlitby
s rozopnutými rukami.

186. Texty tejto eucharistickej
modlitby On v tú noc, keď bol
zradený, Podobne po večeri a Pre-
to, Otče, keď slávime, ako aj za-
končujúcu doxológiu možno spie-
vať.

D) Štvrtá eucharistická modlitba

187. Velebíme t'a, svätý Otče, až
po a všetko posväcuje hovorí len
hlavný celebrant s rozopnutými
rukami.

188. Od Preto ta, Otče, prosíme až
po Zhliadni, Otče, na obetu kon—
celebranti hovoria všetko spolu
takto:

a) Preto t'a, Otče, prosíme s ru—
kami vystretými smerom k darom.

b) Keď nadišla chvíľa aPodobne
vzal kalich s vínom so zopnutými
rukami.

c) Pánove slová vyslovujú s pra—
vicou vystretou smerom k chlebu
a ku kalichu, ak je to vhodné; pri
pozdvihovaní pozrú na hostiu a
na kalich a potom sa hlboko uklo-
ma.

d) Preto, Otče, keď teraz slá—
vime a Zhliadni, Otče, na obetu
hovoria s rozopnutými rukami.

189. Príhovor Pamätaj, Otče, na
všetkých možno zveriť jednému
z koncelebrantov, ktorý prednesie
túto modlitbu s rozopnutými ru—
kami.

19o. Texty tejto eucharistickej
modlitby Keď nadišla chvíľa, Po-
dobne vzal kalich s vínom a Preto,
Otče, keď teraz slávime, ako aj
zakončujúcu doxológiu možno
spievať.

191. Záverečnú doxológiu eucha-
ristickej modlitby prednáša sám

50* VŠEOBECNÉ SMERNICE - MISÁL

