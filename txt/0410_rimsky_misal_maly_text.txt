Vyznanie viery.

NAD OBETNYMI DARMI

Nekonečný Bože, *

nech nám táto eucharistická obeta
stále prináša tvoje požehnanie —

a nech v nás uskutočňuje dielo spásy,
ktoré nám sviatostne sprítomňuje.

Sere Krista, nášho Pána.

Pieseň vďaky nedeľná (str. 374 n.).

SPEV NA PRIJÍMANIE

Pane, tvoja dobrota je nesmierna
a vyhradil si ju bohabojným.

Alebo:

Pán Ježiš povedal:

Blahoslavení, čo šíria pokoj,

lebo ich budú volať Božími synmi.

Blahoslavení, ktorých prenasledujú pre spravodlivosť,
lebo ich je nebeské kráľovstvo.

PO PRIJÍMANÍ

Všemohúci Bože, *

(Ž 30, zo)

(Mt 5, 9-10)

ďakujeme ti, že si nás nasýtil pri svojom stole,

a prosíme t'a, —

aby tento sviatostný pokrm

živil V našich srdciach lásku
a pobáda] nás slúžiť ti

v našich bratoch a sestrách.
Skrze Krista, nášho Pána.

310 OBDOBIE << CEZ ROK »

