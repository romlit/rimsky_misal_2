radostne privítali príchod tvojho Syna 'v sláve,
ako teraz slávime

tajomstvo jeho zmŕtvychvstania.

O to ťa prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Vznešený Bože,

z tvojej dobrotivosti slávime svätú obetu,
ktorou nám dávaš účasť na svojom živote, *
a prosíme ťa, —

pomáhaj nám dôsledne žiť podľa pravdy,
ktorú si nám zjavil.

Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočná (str. 367 n.).

SPEV NA PRIJÍMANIE (Porov. kr 15, 16. 19)

Pán ]ežiš hovorí:

Ja som si vás vyvolil zo sveta

a ustanovil som vás,

aby ste išli a prinášali ovocie

a aby vaše ovocie zostalo. Aleluja.

PO PRIJÍMANÍ

Prosíme t'a, Bože, *

dobrotivo ochraňuj svoj ľud,

ktorý si nasýtil sviatostným pokrmom, —

a pomáhaj mu,

aby prešiel z temnoty hriechu do nového života.
Skrze Krista, nášho Pána.

256 VEĽKONOČNÉ OBDOBIE

