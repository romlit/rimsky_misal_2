O PREBLAHOSLAVENE] PANNE MÁRII I
0 materstve preblahoslavenej Panny Márie

Táto pieseň vďaky sa používa v omšiach o preblahoslavenej Panne Márii.
Na vyznačenom mieste sa pridá dôvod oslavy, ako sa udáva v omšiach
jednotlivých sviatkov.

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

.O.<.0.<.0.<

Je naozaj dôstojné a správne, dobré a Spásonosně
vzdávať vďaky vždy a všade
tebe, Pane, svätý Otče, všemohúci a večný Bože,

a teba....... preblahoslavenej Márie, vždy Panny,
spoločne chváliť, velebit' a oslavovať.

Veď ona počala tvojho jednorodeného Syna,
Duchom Svätým zatônená,

a v ustavičnej sláve panenstva

porodila svetu večné svetlo,

Ježiša Krista, nášho Pána.

Skrze neho tvoju velebu chvália anjeli,

klaňajú sa ti všetky nebeské zástupy,

i blažení setafíni ťa oslavujú spoločným plesaním.
Prosíme, dovoľ i nám, aby sme t'a s nimi

v pokornej úcte chválili:

Svätý, svätý, svätý Pán Boh všetkých svetov...

384 PIESNE VĎAKY

