(držiac veľkonočnú sviecu vo vode, pokračuje):

aby všetci, ktorí v krste tajomne umierajú
a sú pochovaní s-Kristom,

povstali s ním aj k životu.

Skrze Krista, nášho Pána. O.: Amen.

43. Kňaz vytiahne veľkonočnú sviecu z vody. Ľud pritom zvolá:

Oslavujte Pána, pramene,
chváľte a zvelebujte ho naveky!

Možno použiť aj iné zvolanie.

44. Katechumeni sa po jednom osvedčia, že sa zriekajú zlého ducha,
vyznajú vieru a potom prijmú krst. Ak je prítomný biskup alebo kňaz,
ktorý má právomoc birmovať, hneď po krste vysluhuje dospelým novo-
pokrsteným aj sviatosť birmovania.

45. Ak sa nevysluhuje krst a nesvätí sa ani krstná voda, kňaz požehná
vodu touto formulou:

Bratia a sestry,

prosme pokorne Pána Boha, nech posvätí túto vodu,
aby sme ňou mohli byt' pokropení

na pamiatku nášho krstu,

a nech nás milostivo obnoví,

aby sme ostali verní Duchu Svätému,

ktorého sme prijali.

Po krátkej tichej modlitbe pokračuje so zopätými rukami:

Pane a Bože náš, láskavo buď s nami,

keď nábožne slávime túto presvätú noc

a pripomíname si, ako obdivuhodne si nás stvoril
a ešte obdivuhodnejšie vykúpil.

Posvät' túto vodu,

ktorá nám pripomína tvoju starostlivosť o nás:

200 VEĽKONOČNÁ VIGÍLIA

