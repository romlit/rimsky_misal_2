Iné modlitby v omšiach
o Panne Márii

MODLITBA DNA

Všemohúci Bože,

sme šťastní, že si nám dal Pannu Máriu

za mocnú ochrankyňu; *

prosíme t'a, —

na jej materský príhovor zbav

nás všetkého zla na zemi

a priveď nás do večnej radosti v nebi.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

NAD OBETNYMI DARMI

Dobrotivý Bože, *

nech sú ti milé modlitby a obetné dary,
ktoré ti predkladáme pri spomienke

na Božiu Rodičku Pannu Máriu, —

a pre túto svätú obetu nás zahrň

svojím milosrdenstvom.

Skrze Krista, nášho Pána.

PO PRIJÍMANÍ

Láskavý Otče,

pri oslave preblahoslavenej Panny Márie
posilnil si nás sviatosťou spásy; *

daj, prosíme, -—

aby sme ustavične čerpali posilu

z výkupnej obety tvojho Syna.

O to ťa prosíme skrze Krista, nášho Pána.

SVIATKY PANNY MÁRIE 705

