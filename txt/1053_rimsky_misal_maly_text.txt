dával rozličné dary milosti

a v nebeskej sláve si im udelil spoločnú odmenu; *
prosíme ťa,

na ich príhovor daj nám silu, —

aby sme verne plnili povinnosti svojho povolania.
Skrze nášho Pána Ježiša Krista, tvojho Syna...

NAD OBETNYMI DARMI

Presvätý Otče, *

prijmi obetné dary na počesť všetkých svätých,
ktorí už požívajú nehynúcu blaženosť v nebi; —
daj, aby sme pocítili,

že nám účinne pomáhajú na ceste k spáse.
Skrze Krista, nášho Pána.

Pieseň vďaky o svätých (str. 389 n.).

SPEV NA PRIJÍMANIE (Mt 5, 8-10)

Blahoslavení čistého srdca, lebo oni uvidia Boha.

Blahoslaveni, čo šíria pokoj, lebo ich budú volať Božími synmi.
Blahoslavení, ktorých prenasledujú pre spravodlivosť,

lebo ich je nebeské kráľovstvo.

PO PRIJÍMANÍ

Dobrotivý Bože, živíš nás tým istým Chlebom
a udržuješ spoločnou nádejou; *

svojou milosťou nás posilňuj, —

aby sme boli všetci, spolu s tvojimi svätými,
jedno telo a jeden duch v Kristovi

a spojení s ním boli vzkriesení k večnej sláve.
Skrze Krista, nášho Pána.

O VSETKYCH SVÁTYCH 953

